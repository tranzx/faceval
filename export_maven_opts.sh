#!/bin/sh

# export MAVEN_OPTS if you see OutOfMemoryError: PermGen space
# while testing JasperReports reports.
export MAVEN_OPTS="-Xmx1024m -XX:PermSize=256m -XX:MaxPermSize=512m"