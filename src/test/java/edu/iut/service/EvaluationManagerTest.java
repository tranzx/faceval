package edu.iut.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.iut.model.Evaluation;
import edu.iut.model.Question;
import edu.iut.model.Semester;

public class EvaluationManagerTest extends BaseManagerTestCase {

	@Autowired private EvaluationManager mgr;
	@Autowired private QuestionManager questionManager;

	private Evaluation evaluation;

	@Test
	public void testSaveEvaluation() throws Exception {
		evaluation = mgr.findByYearAndSemester("1392", Semester.FIRST);

		assertNotNull(evaluation);

		Question q1 = questionManager.get(new Long("-1"));
		Question q2 = questionManager.get(new Long("-2"));
		Set<Question> questions = new HashSet<Question>();
		questions.add(q1);
		evaluation.setQuestions(questions);

		Evaluation saved = mgr.save(evaluation);
		assertEquals(1, saved.getQuestions().size());
		assertEquals(new Long("-1"), saved.getQuestions().iterator().next().getId());

		questions.clear();
		questions.add(q2);
		evaluation.setQuestions(questions);
		Evaluation saved2 = mgr.save(evaluation);
		assertEquals(1, saved2.getQuestions().size());
		assertEquals(new Long("-2"), saved2.getQuestions().iterator().next().getId());
	}

}