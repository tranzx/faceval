package edu.iut.dao;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class EvaluationDaoTest extends BaseDaoTestCase {

	@Autowired private EvaluationDao dao;

	@Test
	public void testFindCurrentActiveEvaluation() {
		assertNotNull(dao.findCurrentActiveEvaluation());
	}

}