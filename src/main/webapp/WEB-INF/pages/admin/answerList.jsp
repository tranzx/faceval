<%@ include file="/common/taglibs.jsp" %>

<head>
  <title><fmt:message key="answerList.title"/></title>
  <meta name="menu" content="BaseInfoMenu"/>
</head>

<div class="span10">
  <h2><fmt:message key="answerList.heading"/></h2>

  <display:table name="answerList" id="answer" cellpadding="0" cellspacing="0"
                 defaultsort="1" class="table table-condesed table-striped table-hover" pagesize="10" requestURI="">
    <display:column titleKey="answer.student" property="student" sortable="true"/>
    <display:column titleKey="answer.faculty" property="faculty" sortable="true"/>
    <display:column titleKey="answer.question" property="question" sortable="true"/>
    <fmt:message key="${answer.level}" var="level" />
    <display:column titleKey="answer.level" sortable="true">
      ${level}
    </display:column>
    <display:column titleKey="button.delete">
      <form:form commandName="answerDform" method="delete" action="/admin/answer/${answer.id}" id="answerDelete">

        <c:set var="delObject" scope="request"><fmt:message key="answerList.answer"/></c:set>
        <script type="text/javascript">var msgDelConfirm =
          "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
        </script>

        <button type="submit" class="btn btn-danger" onclick="return confirmMessage(msgDelConfirm)">
          <i class="icon-remove"><!-- --></i> <fmt:message key="button.delete"/>
        </button>
      </form:form>
    </display:column>
  </display:table>
</div>