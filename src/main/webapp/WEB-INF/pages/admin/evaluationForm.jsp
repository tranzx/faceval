<%@ include file="/common/taglibs.jsp"%>

<head>
  <title><fmt:message key="evaluationForm.title"/></title>
  <meta name="menu" content="EvalMenu"/>
</head>

<div class="span10">
  <spring:bind path="evaluation.*">
    <c:if test="${not empty status.errorMessages}">
      <div class="alert alert-error fade in">
        <a href="#" data-dismiss="alert" class="close">&times;</a>
        <c:forEach var="error" items="${status.errorMessages}">
          <c:out value="${error}" escapeXml="false"/><br/>
        </c:forEach>
      </div>
    </c:if>
  </spring:bind>

  <form:form commandName="evaluation" method="post" action="/admin/evaluationform" id="evaluationForm" autocomplete="off"
             cssClass="well form-horizontal">

    <spring:bind path="evaluation.name">
    <fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
    </spring:bind>
      <appfuse:label styleClass="control-label" key="evaluation.name"/>
      <div class="controls">
        <form:input path="name" id="name" />
        <form:errors path="name" cssClass="help-inline"/>
      </div>
    </fieldset>

    <spring:bind path="evaluation.active">
    <fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
    </spring:bind>
      <appfuse:label styleClass="control-label" key="evaluation.active"/>
      <div class="controls">
        <form:checkbox path="active" id="active" />
        <form:errors path="active" cssClass="help-inline" />
      </div>
    </fieldset>

    <spring:bind path="evaluation.year">
    <fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
    </spring:bind>
      <appfuse:label styleClass="control-label" key="evaluation.year"/>
      <div class="controls">
        <form:select path="year" id="year">
          <c:forEach var="i" begin="1392" end="1400">
            <form:option value="${i}">${i}</form:option>
          </c:forEach>
        </form:select>
        <form:errors path="year" cssClass="help-inline"/>
      </div>
    </fieldset>

    <spring:bind path="evaluation.semester">
    <fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
    </spring:bind>
      <appfuse:label styleClass="control-label" key="evaluation.semester"/>
      <div class="controls">
        <form:select path="semester" items="${semesters}" />
        <form:errors path="semester" cssClass="help-inline"/>
      </div>
    </fieldset>


    <fieldset class="control-group">
      <label for="evalQuestions" class="control-label"><fmt:message key="evaluation.questions"/></label>
      <div class="controls">
        <select id="evalQuestions" name="evalQuestions" multiple="true" size="9" class="input-xxlarge">
          <c:forEach items="${questionSet}" var="question">
            <c:set value="" var="selected" />
            <c:forEach items="${evaluation.questions}" var="selectedQ">
              <c:if test="${selectedQ.id == question.id}">
                <c:set value="selected" var="selected"/>
              </c:if>
            </c:forEach>
            <option value="${question.id}" ${selected}>${question.text}</option>
          </c:forEach>
        </select>
      </div>
    </fieldset>
    
    <fieldset class="control-group">
      <label for="evalFaculties" class="control-label"><fmt:message key="evaluation.faculties"/></label>
      <div class="controls">
        <select id="evalFaculties" name="evalFaculties" multiple="true" size="9" class="input-xxlarge">
          <c:forEach items="${facultySet}" var="faculty">
            <c:set value="" var="selected" />
            <c:forEach items="${evaluation.faculties}" var="selectedF">
              <c:if test="${selectedF.id == faculty.id}">
                <c:set value="selected" var="selected"/>
              </c:if>
            </c:forEach>
            <option value="${faculty.id}" ${selected}>${faculty}</option>
          </c:forEach>
        </select>
      </div>
    </fieldset>
    
    
    
    

    <fieldset class="form-actions">
      <button type="submit" class="btn btn-primary" name="save">
        <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
      </button>
    </fieldset>

    <input type="hidden" name="id" value="${evaluation.id}" />
    <input type="hidden" name="version" value="${evaluation.version}" />
  </form:form>

</div>