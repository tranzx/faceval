<%@ include file="/common/taglibs.jsp" %>

<head>
  <title><fmt:message key="timeList.title"/></title>
  <meta name="menu" content="BaseInfoMenu"/>
</head>

<div class="span10">
  <h2><fmt:message key="timeList.heading"/></h2>

  <div id="actions" class="form-actions">
    <a class="btn btn-primary" href="<c:url value='/admin/timeform'/>">
      <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/>
    </a>
  </div>

  <display:table name="timeList" cellspacing="0" cellpadding="0" requestURI=""
                 defaultsort="1" id="times" pagesize="25" export="false"
                 class="table table-condensed table-striped table-hover">
    <display:column property="term" escapeXml="true" sortable="true" titleKey="time.term"
                    style="width: 25%"/>
    <display:column property="year" escapeXml="true" sortable="true" titleKey="time.year"
                    style="width: 25%"/>
 
  </display:table>
</div>