<%@ include file="/common/taglibs.jsp" %>

<head>
  <title><fmt:message key="groupClassList.title"/></title>
  <meta name="menu" content="BaseInfoMenu"/>
</head>

<div class="span10">
  <h2><fmt:message key="groupClassList.heading"/></h2>

  <div id="actions" class="form-actions">
    <a class="btn btn-primary" href="<c:url value='/admin/groupClassform'/>">
      <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/>
    </a>
  </div>

  <display:table name="groupClassList" cellspacing="0" cellpadding="0" requestURI=""
                 defaultsort="1" id="groupClasses" pagesize="25" export="false"
                 class="table table-condensed table-striped table-hover">
    <display:column property="name" escapeXml="true" sortable="true" titleKey="groupClass.name"
                    style="width: 25%"/>
    <display:column property="groupEducation" escapeXml="true" sortable="true" titleKey="groupClass.groupEducation"
                    style="width: 25%"/>
                    
     <display:column titleKey="button.delete">
      <form:form commandName="groupClassDform" method="delete" action="/admin/groupClass/${groupClasses.id}" id="groupClassDelete">

        <c:set var="delObject" scope="request"><fmt:message key="groupClassList.groupClass"/></c:set>
        <script type="text/javascript">var msgDelConfirm =
          "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
        </script>

        <button type="submit" class="btn btn-danger" onclick="return confirmMessage(msgDelConfirm)">
          <i class="icon-remove"><!-- --></i> <fmt:message key="button.delete"/>
        </button>
      </form:form>
    </display:column>
  </display:table>
</div>