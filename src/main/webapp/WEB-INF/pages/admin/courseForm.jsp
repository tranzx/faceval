<%@ include file="/common/taglibs.jsp"%>

<head>
  <title><fmt:message key="courseForm.title"/></title>
  <meta name="menu" content="BaseInfoMenu"/>
</head>

<div class="span10">
  <spring:bind path="course.*">
    <c:if test="${not empty status.errorMessages}">
      <div class="alert alert-error fade in">
        <a href="#" data-dismiss="alert" class="close">&times;</a>
        <c:forEach var="error" items="${status.errorMessages}">
          <c:out value="${error}" escapeXml="false"/><br />
        </c:forEach>
      </div>
    </c:if>
  </spring:bind>

  <form:form commandName="course" method="post" action="/admin/courseform"
             id="courseForm" autocomplete="off">

    <spring:bind path="course.name">
    <fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
    </spring:bind>
      <appfuse:label styleClass="control-label" key="course.name"/>
      <div class="controls">
        <form:input path="name" id="name" maxlength="50"/>
        <form:errors path="name" cssClass="help-inline"/>
      </div>
    </fieldset>


    <fieldset class="form-actions">
      <button type="submit" class="btn btn-primary" name="save">
        <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
      </button>
    </fieldset>

  </form:form>
</div>