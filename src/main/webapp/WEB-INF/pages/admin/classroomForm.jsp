<%@ include file="/common/taglibs.jsp"%>

<head>
<title><fmt:message key="classroomForm.title" /></title>
<meta name="menu" content="BaseInfoMenu" />
</head>

<div class="span10">
	<spring:bind path="classroom.*">
		<c:if test="${not empty status.errorMessages}">
			<div class="alert alert-error fade in">
				<a href="#" data-dismiss="alert" class="close">&times;</a>
				<c:forEach var="error" items="${status.errorMessages}">
					<c:out value="${error}" escapeXml="false" />
					<br />
				</c:forEach>
			</div>
		</c:if>
	</spring:bind>

	<form:form commandName="classroom" method="post"
		action="/admin/classroomform" id="classroomForm" autocomplete="off">

		<spring:bind path="classroom.groupClass">
			<fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
		</spring:bind>
		<appfuse:label styleClass="control-label" key="classroom.groupClass" />

			<div class="controls">
				<form:select path="groupClass" items="${groupClassItem}" itemLabel="name" />
				<form:errors path="groupClass" cssClass="help-inline" />
			</div>
		</fieldset>



		<spring:bind path="classroom.faculty">
			<fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
		</spring:bind>
		<appfuse:label styleClass="control-label" key="classroom.faculty" />

			<div class="controls">
				<form:select path="faculty" items="${facultyItem}" itemLabel="fullname" />
				<form:errors path="faculty" cssClass="help-inline" />
			</div>
		</fieldset>

		<spring:bind path="classroom.course">
			<fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
		</spring:bind>
		<appfuse:label styleClass="control-label" key="classroom.course" />

			<div class="controls">
				<form:select path="course" items="${courseItem}" itemLabel="name" />
				<form:errors path="course" cssClass="help-inline" />
			</div>
		</fieldset>


		<fieldset class="form-actions">
			<button type="submit" class="btn btn-primary" name="save">
				<i class="icon-ok icon-white"></i>
				<fmt:message key="button.save" />
			</button>
		</fieldset>

	</form:form>
</div>