<%@ include file="/common/taglibs.jsp"%>

<head>
  <title><fmt:message key="timeForm.title"/></title>
  <meta name="menu" content="BaseInfoMenu"/>
</head>

<div class="span10">
  <spring:bind path="time.*">
    <c:if test="${not empty status.errorMessages}">
      <div class="alert alert-error fade in">
        <a href="#" data-dismiss="alert" class="close">&times;</a>
        <c:forEach var="error" items="${status.errorMessages}">
          <c:out value="${error}" escapeXml="false"/><br />
        </c:forEach>
      </div>
    </c:if>
  </spring:bind>

  <form:form commandName="time" method="post" action="/admin/timeform"
             id="timeForm" autocomplete="off">

    <spring:bind path="time.term">
    <fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
    </spring:bind>
      <appfuse:label styleClass="control-label" key="time.term"/>
      <div class="controls">
        <form:input path="term" id="term" maxlength="50"/>
        <form:errors path="term" cssClass="help-inline"/>
      </div>
    </fieldset>

    <spring:bind path="time.year">
    <fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
    </spring:bind>
      <appfuse:label styleClass="control-label" key="time.year"/>
      <div class="controls">
        <form:input path="year" id="year" maxlength="50"/>
        <form:errors path="year" cssClass="help-inline"/>
      </div>
    </fieldset>

    <fieldset class="form-actions">
      <button type="submit" class="btn btn-primary" name="save">
        <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
      </button>
    </fieldset>

  </form:form>
</div>