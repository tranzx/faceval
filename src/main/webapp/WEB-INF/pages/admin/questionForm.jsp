<%@ include file="/common/taglibs.jsp"%>

<head>
  <title><fmt:message key="questionForm.title"/></title>
  <meta name="menu" content="BaseInfoMenu"/>
</head>

<div class="span10">
   <spring:bind path="question.*">
     <c:if test="${not empty status.errorMessages}">
       <div class="alert alert-error fade in">
         <a href="#" data-dismiss="alert" class="close">&times;</a>
         <c:forEach var="error" items="${status.errorMessages}">
           <c:out value="error" escapeXml="false"/><br />
         </c:forEach>
       </div>
     </c:if>
   </spring:bind>

  <form:form commandName="question" method="post" action="/admin/questionform"
             id="questionForm" autocomplete="off">

  <spring:bind path="question.text">
    <fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
  </spring:bind>
    <appfuse:label styleClass="control-label" key="question.text"/>
    <div class="controls">
      <form:input path="text" id="name" maxlength="250"/>
      <form:errors path="text" cssClass="help-inline"/>
    </div>

	   <spring:bind path="question.questionType">
    <fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
    </spring:bind>
      <appfuse:label styleClass="control-label" key="question.questionType"/>
      <div class="controls">
        <form:select path="questionType" items="${questionTypeItem}" id="questionType" cssClass="input-xlarge" itemLabel="name" />
        <form:errors path="questionType" cssClass="help-inline"/>
      </div>
    </fieldset>

    <fieldset class="form-actions">
      <button type="submit" class="btn btn-primary" name="save">
        <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
      </button>
    </fieldset>
  </form:form>

</div>