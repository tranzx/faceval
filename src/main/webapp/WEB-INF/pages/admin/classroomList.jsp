<%@ include file="/common/taglibs.jsp" %>

<head>
  <title><fmt:message key="classroomList.title"/></title>
  <meta name="menu" content="BaseInfoMenu"/>
</head>

<div class="span10">
  <h2><fmt:message key="classroomList.heading"/></h2>

  <div id="actions" class="form-actions">
    <a class="btn btn-primary" href="<c:url value='/admin/classroomform'/>">
      <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/>
    </a>
  </div>

  <display:table name="classroomList" cellspacing="0" cellpadding="0" requestURI=""
                 defaultsort="1" id="classrooms" pagesize="25" export="false"
                 class="table table-condensed table-striped table-hover">
    <display:column property="groupClass" escapeXml="true" sortable="true" titleKey="classroom.groupClass"
                    style="width: 25%"/>
    <display:column property="faculty" escapeXml="true" sortable="true" titleKey="classroom.faculty"
                    style="width: 25%"/>
    <display:column property="course" escapeXml="true" sortable="true" titleKey="classroom.course"
                    style="width: 25%"/>
      <display:column titleKey="button.delete">
      <form:form commandName="classroomDform" method="delete" action="/admin/classroom/${classrooms.id}" id="classroomDelete">

        <c:set var="delObject" scope="request"><fmt:message key="classroomList.classroom"/></c:set>
        <script type="text/javascript">var msgDelConfirm =
          "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
        </script>

        <button type="submit" class="btn btn-danger" onclick="return confirmMessage(msgDelConfirm)">
          <i class="icon-remove"><!-- --></i> <fmt:message key="button.delete"/>
        </button>
      </form:form>
    </display:column> 

  </display:table>
</div>