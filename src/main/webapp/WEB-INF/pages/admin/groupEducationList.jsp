<%@ include file="/common/taglibs.jsp" %>

<head>
  <title><fmt:message key="groupEducationList.title"/></title>
  <meta name="menu" content="BaseInfoMenu"/>
</head>

<div class="span10">
  <h2><fmt:message key="groupEducationList.heading"/></h2>

  <div id="actions" class="form-actions">
    <a class="btn btn-primary" href="<c:url value='/admin/groupEducationform'/>">
      <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/>
    </a>
  </div>

  <display:table name="groupEducationList" cellspacing="0" cellpadding="0" requestURI=""
                 defaultsort="1" id="groupEducations" pagesize="25" export="false"
                 class="table table-condensed table-striped table-hover">
    <display:column property="name" escapeXml="true" sortable="true" titleKey="groupEducation.name"
                    style="width: 25%"/>
                    
     <display:column titleKey="button.delete">
      <form:form commandName="groupEducationDform" method="delete" action="/admin/groupEducation/${groupEducations.id}" id="groupEducationsDelete">

        <c:set var="delObject" scope="request"><fmt:message key="groupEducationList.groupEducation"/></c:set>
        <script type="text/javascript">var msgDelConfirm =
          "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
        </script>

        <button type="submit" class="btn btn-danger" onclick="return confirmMessage(msgDelConfirm)">
          <i class="icon-remove"><!-- --></i> <fmt:message key="button.delete"/>
        </button>
      </form:form>
    </display:column>              
  </display:table>
</div>