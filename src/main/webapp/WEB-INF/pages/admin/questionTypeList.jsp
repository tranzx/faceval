<%@ include file="/common/taglibs.jsp" %>

<head>
  <title><fmt:message key="questionTypeList.title"/></title>
  <meta name="menu" content="BaseInfoMenu"/>
</head>

<div class="span10">
  <h2><fmt:message key="questionTypeList.heading"/></h2>

  <div id="actions" class="form-actions">
    <a class="btn btn-primary" href="<c:url value='/admin/questionTypeform'/>">
      <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/>
    </a>
  </div>

  <display:table name="questionTypeList" cellspacing="0" cellpadding="0" requestURI=""
                 defaultsort="1" id="questionTypes" pagesize="25" export="false"
                 class="table table-condensed table-striped table-hover">
    <display:column property="name" escapeXml="true" sortable="true" titleKey="questionType.name"
                    style="width: 25%"/>
                    
      <display:column titleKey="button.delete">
      <form:form commandName="questionTypeDform" method="delete" action="/admin/questionType/${questionTypes.id}" id="questionTypeDelete">

        <c:set var="delObject" scope="request"><fmt:message key="questionTypeList.questionType"/></c:set>
        <script type="text/javascript">var msgDelConfirm =
          "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
        </script>

        <button type="submit" class="btn btn-danger" onclick="return confirmMessage(msgDelConfirm)">
          <i class="icon-remove"><!-- --></i> <fmt:message key="button.delete"/>
        </button>
      </form:form>
    </display:column>              
     
                    
  </display:table>
</div>