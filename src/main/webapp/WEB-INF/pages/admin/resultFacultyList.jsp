<%@ include file="/common/taglibs.jsp" %>

<head>
  <title><fmt:message key="resultFacultyList.title"/></title>
  <meta name="menu" content="EvalMenu"/>
</head>

<div class="span10">
  <h2><fmt:message key="resultFacultyList.heading"/></h2>

  <div id="actions" class="form-actions">
    <a class="btn" href="<c:url value='/admin/resultfaculty/reload'/>">
      <i class="icon-refresh"><!-- --></i>
      <fmt:message key="button.refresh"/>
    </a>
    <form:form commandName="facultyQuestionFormBean" method="post" action="/admin/resultfaculty/filter"
               id="resultFacultyForm" cssClass="well form-horizontal">
      <fieldset class="control-group">
        <appfuse:label styleClass="control-label" key="resultFacultyList.question"/>
        <form:select path="question" id="question" cssClass="input-xxlarge">
          <form:option value="null">----------</form:option>
          <form:options items="${questions}" itemLabel="text"/>
        </form:select>
      </fieldset>
      <fieldset class="control-group">
        <appfuse:label styleClass="control-label" key="resultFacultyList.faculty"/>
        <form:select path="faculty" id="faculty" >
          <form:option value="null">----------</form:option>
          <form:options items="${faculties}" itemLabel="fullname"/>
        </form:select>
        <button class="btn btn-primary" type="submit">
          <i class="icon-ok-sign icon-white"><!-- --></i>
          <fmt:message key="button.apply"/>
        </button>
      </fieldset>
    </form:form>
  </div>

  <display:table name="faaList" id="faaList" defaultsort="1" pagesize="10"
                 class="table table-condesed table-striped table-hover" requestURI="">
    <display:column titleKey="resultFacultyList.faculty" property="faculty" sortable="true"/>
    <display:column titleKey="resultFacultyList.answerCount" property="answerCount" sortable="true"/>
    <display:column titleKey="resultFacultyList.levelAvg" property="levelAvg" sortable="true"/>
  </display:table>

</div>