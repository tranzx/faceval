<%@ include file="/common/taglibs.jsp" %>

<head>
  <title><fmt:message key="resultStudentList.title"/></title>
  <meta name="menu" content="EvalMenu"/>
</head>

<div class="span10">
  <h2><fmt:message key="resultStudentList.heading"/></h2>

  <display:table name="resultStudentList" id="rsList" defaultsort="1" pagesize="10"
                 class="table table-condesed table-striped table-hover" requestURI="">
    <display:column titleKey="resultStudentList.student" property="fullname" sortable="true"/>
  </display:table>

</div>