<%@ include file="/common/taglibs.jsp" %>

<head>
  <title><fmt:message key="evaluationList.title"/></title>
  <meta name="menu" content="EvalMenu"/>
</head>

<div class="span10">
  <h2><fmt:message key="evaluationList.heading"/></h2>

  <div id="actions" class="form-actions">
    <a class="btn btn-primary" href="<c:url value='/admin/evaluationform'/>">
      <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/></a>
  </div>

  <display:table name="evaluationList" cellspacing="0" cellpadding="0" requestURI=""
                 defaultsort="1" id="evals" pagesize="25" export="false"
                 class="table table-condensed table-striped table-hover">

    <display:column property="name" escapeXml="true" sortable="true" titleKey="evaluation.name"
                    url="/admin/evaluationEditform" paramId="id" paramProperty="id" style="width: 25%"/>
    <display:column property="year" escapeXml="true" sortable="true" titleKey="evaluation.year"
                     style="width: 25%"/>
    <fmt:message key="${evals.semester}" var="semesterLabel" />
    <display:column value="${semesterLabel}" escapeXml="true" sortable="true" titleKey="evaluation.semester"
                     style="width: 25%"/>
    <display:column property="questions" escapeXml="true" sortable="true" titleKey="evaluation.questions"
                    style="width: 25%"/>
    <display:column property="faculties" escapeXml="true" sortable="true" titleKey="evaluation.faculties"
                    style="width: 25%"/>

    <display:column titleKey="evaluation.active">
      <c:if test="${evals.active}">
        <button class="btn btn-success">
          <i class="icon-plus-sign"><!-- --></i>
          <fmt:message key="evaluation.active"/>
        </button>
      </c:if>
    </display:column>
    <display:column titleKey="button.delete">
      <form:form commandName="evaluation" method="delete" action="/admin/evaluation?id=${evals.id}" id="evaluationDelete">
        <button type="submit" class="btn btn-danger">
          <i class="icon-remove"><!-- --></i> <fmt:message key="button.delete"/>
        </button>
      </form:form>
    </display:column>

  </display:table>

</div>