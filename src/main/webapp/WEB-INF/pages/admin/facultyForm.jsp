<%@ include file="/common/taglibs.jsp"%>

<head>
  <title><fmt:message key="facultyForm.title"/></title>
  <meta name="menu" content="BaseInfoMenu"/>
</head>

<div class="span10">
  <spring:bind path="faculty.*">
    <c:if test="${not empty status.errorMessages}">
      <div class="alert alert-error fade in">
        <a href="#" data-dismiss="alert" class="close">&times;</a>
        <c:forEach var="error" items="${status.errorMessages}">
          <c:out value="${error}" escapeXml="false"/><br />
        </c:forEach>
      </div>
    </c:if>
  </spring:bind>

  <form:form commandName="faculty" method="post" action="/admin/facultyform"
             id="facultyForm" autocomplete="off">

    <spring:bind path="faculty.name">
    <fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
    </spring:bind>
      <appfuse:label styleClass="control-label" key="faculty.name"/>
      <div class="controls">
        <form:input path="name" id="name" maxlength="50"/>
        <form:errors path="name" cssClass="help-inline"/>
      </div>
    </fieldset>

    <spring:bind path="faculty.family">
    <fieldset class="control-group${(not empty status.errorMessage) ? ' error' : ''}">
    </spring:bind>
      <appfuse:label styleClass="control-label" key="faculty.family"/>
      <div class="controls">
        <form:input path="family" id="family" maxlength="50"/>
        <form:errors path="family" cssClass="help-inline"/>
      </div>
    </fieldset>

    <fieldset class="form-actions">
      <button type="submit" class="btn btn-primary" name="save">
        <i class="icon-ok icon-white"></i> <fmt:message key="button.save"/>
      </button>
    </fieldset>

  </form:form>
</div>