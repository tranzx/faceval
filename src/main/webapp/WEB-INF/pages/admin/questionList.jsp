<%@ include file="/common/taglibs.jsp" %>

<head>
  <title><fmt:message key="questionList.title"/><title>
  <meta name="menu" content="BaseInfoMenu"/>
</head>

<div class="span10">
  <h2><fmt:message key="questionList.heading"/></h2>

  <div id="actions" class="form-actions">
    <a class="btn btn-primary" href="<c:url value='/admin/questionform'/>">
      <i class="icon-plus icon-white"></i> <fmt:message key="button.add"/>
    </a>
  </div>

  <display:table name="questionList" cellspacing="0" cellpadding="0" requestURI=""
                 defaultsort="1" id="questions" pagesize="5" export="false"
                 class="table table-condensed table-striped table-hover">
    <display:column property="text" escapeXml="true" sortable="true" titleKey="question.text"
                    style="width: 25%"/>
    <display:column property="questionType" escapeXml="true" sortable="true" titleKey="question.questionType"
                    style="width: 25%"/>
     <display:column titleKey="button.delete" style="width: 25%">
      <form:form commandName="questionDform" method="delete" action="/admin/question/${questions.id}" id="questionDelete">

        <c:set var="delObject" scope="request"><fmt:message key="questionList.question"/></c:set>
        <script type="text/javascript">var msgDelConfirm =
          "<fmt:message key="delete.confirm"><fmt:param value="${delObject}"/></fmt:message>";
        </script>

        <button type="submit" class="btn btn-danger" onclick="return confirmMessage(msgDelConfirm)">
          <i class="icon-remove"><!-- --></i> <fmt:message key="button.delete"/>
        </button>
      </form:form>
    </display:column>              
                    
  </display:table>

</div>