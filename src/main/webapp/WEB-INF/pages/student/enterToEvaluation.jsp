<%@ include file="/common/taglibs.jsp" %>

<head>
  <title><fmt:message key="questionList.title"/><title>
  <meta name="menu" content="StudentMenu"/>
</head>

<div class="span10">
  <h2><fmt:message key="evaluation.questionsForFaculty" ><fmt:param value ="${selectedFaculty}"/></fmt:message></h2>

  <form:form commandName="submitEvaluation" method="post" action="/student/submit/${selectedFaculty.id}" class="well form-horizontal" id="submitEvaluation">
  <c:forEach items="${questionList}" var="question">
    <fieldset class="control-group">
      <label class="question-label">${question}</label>
      <div class="controls">
	    <div class="inline-block">
	      <label for="${question.id}1" class="inline-block">
	          <fmt:message key="evaluation.little" />
	      </label>
	      <input type="radio" name="answer${question.id}" id="${question.id}1" value="WEAK">
	    </div>
	    <div class="inline-block">
	      <label for="${question.id}2" class="inline-block" style="padding-right: 10px">
	          <fmt:message key="evaluation.middle" />
	      </label>
	      <input type="radio" name="answer${question.id}" id="${question.id}2" value="MIDDLE">
	    </div>
	    <div style="display:inline-block;">
	      <label for="${question.id}3" class="inline-block" style="padding-right: 10px">
	          <fmt:message key="evaluation.good" />
	      </label>
	      <input type="radio" name="answer${question.id}" id="${question.id}3" value="GOOD">
	    </div>
	    <div style="display:inline-block;">
	      <label for="${question.id}4" class="inline-block" style="padding-right: 10px">
	          <fmt:message key="evaluation.verygood" />
	      </label>
	      <input type="radio" name="answer${question.id}" id="${question.id}4" value="VERYGOOD">
	    </div>
	    <div style="display:inline-block;">
	      <label for="${question.id}5" class="inline-block" style="padding-right: 10px">
	          <fmt:message key="evaluation.great" />
	      </label>
	      <input type="radio" name="answer${question.id}" id="${question.id}5" value="GREAT">
	    </div>
      </div>
    </fieldset>
  </c:forEach>

  <div id="actions" class="form-actions">
    <button type="submit" class="btn btn-primary" name="save">
       <i class="icon-ok icon-white"><!-- --></i> <fmt:message key="button.save"/>
    </button>
  </div>

  </form:form>

</div>