<%@ include file="/common/taglibs.jsp" %>

<head>
  <title><fmt:message key="facultyList.title"/></title>
  <meta name="menu" content="StudentMenu"/>
</head>

<div class="span10">
  <h2><fmt:message key="facultyList.heading"/></h2>

  <display:table name="facultyList" cellspacing="0" cellpadding="0" requestURI=""
                 defaultsort="1" id="faculties" pagesize="10" export="false"
                 class="table table-condensed table-striped table-hover">
    <display:column property="name" escapeXml="true" sortable="true" titleKey="faculty.name"
                    style="width: 25%"/>
    <display:column property="family" escapeXml="true" sortable="true" titleKey="faculty.family"
                    style="width: 25%"/>
    <fmt:message key="enter.evaluation" var="enter_evaluation"/>
    <display:column title="${enter_evaluation}" url="/student/enter" paramId="id" paramProperty="id">
        ${enter_evaluation}
    </display:column>

  </display:table>
</div>