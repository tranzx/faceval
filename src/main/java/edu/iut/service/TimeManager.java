package edu.iut.service;

import edu.iut.model.Time;

/**
 * Business service interface to handle communication between web and
 * domain layer.
 *
 */
public interface TimeManager extends GenericManager<Time, Long> {

}