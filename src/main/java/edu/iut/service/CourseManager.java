package edu.iut.service;

import edu.iut.model.Course;

/**
 * Business service interface to handle communication between web and
 * domain layer.
 *
 */
public interface CourseManager extends GenericManager<Course, Long> {

}