package edu.iut.service.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.iut.model.Answer;
import edu.iut.model.Faculty;
import edu.iut.model.Question;

public class FacultyAnswersAggregate {

	private Faculty faculty;
	private Set<Answer> answers;

	public float getLevelAvg() {
		float sum = 0;
		int count = 0;
		for (Answer answer: getAnswers()) {
			int level = answer.getLevel().num();
			sum += level;
			count++;
		}
		return sum/count;
	}

	public int getAnswerCount() {
		return getAnswers().size();
	}

	public static List<FacultyAnswersAggregate> filterWithFaculty(List<FacultyAnswersAggregate> list, Faculty faculty) {
		List<FacultyAnswersAggregate> result = new ArrayList<FacultyAnswersAggregate>();
		for (FacultyAnswersAggregate one: list) {
			if (one.getFaculty().equals(faculty)) {
				result.add(one);
			}
		}
		return result;
	}

	public static List<FacultyAnswersAggregate> filterWithQuestion(List<FacultyAnswersAggregate> aggregates, Question question) {
		List<FacultyAnswersAggregate> result = new ArrayList<FacultyAnswersAggregate>();

		for (FacultyAnswersAggregate one: aggregates) {
			for (Answer answer: one.getAnswers()) {
				if (answer.getQuestion().equals(question)) {
					result.add(facultyAnswerAggregate(one.getFaculty(), answer));
				}
			}
		}
		return result;
	}

	public static List<FacultyAnswersAggregate> filterWithFacultyAndQuestion(List<FacultyAnswersAggregate> list, Faculty faculty, Question question) {
		List<FacultyAnswersAggregate> filterredWithFaculty = filterWithFaculty(list, faculty);
		return filterWithQuestion(filterredWithFaculty, question);
	}

	public Faculty getFaculty() {
		return faculty;
	}

	public Set<Answer> getAnswers() {
		return answers;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}

	public void setAnswers(Set<Answer> answers) {
		this.answers = answers;
	}

	// -- private methods.

	private static FacultyAnswersAggregate facultyAnswerAggregate(Faculty faculty, Answer answer) {
		FacultyAnswersAggregate aggregate = new FacultyAnswersAggregate();
		aggregate.setFaculty(faculty);
		aggregate.setAnswers(new HashSet<Answer>());
		aggregate.getAnswers().add(answer);
		return aggregate;
	}

}