package edu.iut.service;

import java.util.List;

import edu.iut.model.Classroom;
import edu.iut.model.GroupClass;

/**
 * Business service interface to handle communication between web and
 * domain layer.
 *
 */
public interface ClassroomManager extends GenericManager<Classroom, Long> {

	List<Classroom> findByGroupClass(GroupClass groupClass);

}