package edu.iut.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import edu.iut.model.Faculty;

public class StudentFacultyIdentityMap {

	Map<String, Set<Faculty>> identityMap = null;

	public void put(String username, Set<Faculty> faculties) {
		getIdentityMap().put(username, faculties);
	}

	public Set<Faculty> get(String username) {
		return getIdentityMap().get(username);
	}

	public boolean contains(String username) {
		return getIdentityMap().containsKey(username);
	}

	public Map<String, Set<Faculty>> getIdentityMap() {
		if (identityMap == null) {
			identityMap = new HashMap<String, Set<Faculty>>();
		}
		return identityMap;
	}

}