package edu.iut.service;

import java.util.List;

import edu.iut.model.Evaluation;
import edu.iut.model.Semester;

/**
 * Business service interface to handle communication between web and
 * domain layer.
 *
 */
public interface EvaluationManager extends GenericManager<Evaluation, Long> {

	List<Evaluation> getEvlauations();

	Evaluation findByYearAndSemester(String year, Semester semester);

	Evaluation findCurrentActiveEvaluation();

}