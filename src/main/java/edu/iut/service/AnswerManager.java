package edu.iut.service;

import java.util.List;

import edu.iut.model.Answer;
import edu.iut.model.Evaluation;

public interface AnswerManager extends GenericManager<Answer, Long> {

	List<Answer> getAllDistinct();

	List<Answer> findByEvaluation(Evaluation evaluation);

	List<Answer> findAnswersForCurrentEvaluation();
}