package edu.iut.service;

import edu.iut.model.Question;

/**
 * Business service interface to handle communication between web and
 * domain layer.
 */
public interface QuestionManager extends GenericManager<Question, Long> {

}