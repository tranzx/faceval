package edu.iut.service;

import java.util.HashSet;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import edu.iut.model.Answer;
import edu.iut.model.Evaluation;
import edu.iut.model.Faculty;
import edu.iut.service.dto.FacultyAnswersAggregate;

@Service("facultyService")
public class FacultyService implements InitializingBean {

	@Autowired private AnswerManager answerManager;
	@Autowired private EvaluationManager evaluationManager;

	List<FacultyAnswersAggregate> facultyAnswersAggregates = null;

	@Override
	public void afterPropertiesSet() throws Exception {
		Evaluation evaluation = evaluationManager.findCurrentActiveEvaluation();
		List<Answer> answers = answerManager.findByEvaluation(evaluation);

		facultyAnswersAggregates = aggreateFacultyAnswers(answers); // Initializing
	}

	public List<FacultyAnswersAggregate> currentFacultyAnswersAggregate() {
		return facultyAnswersAggregates;
	}

	public void reloadFacultyAnswerAggregate() {
		Evaluation evaluation = evaluationManager.findCurrentActiveEvaluation();
		List<Answer> answers = answerManager.findByEvaluation(evaluation);

		facultyAnswersAggregates = null;
		facultyAnswersAggregates = aggreateFacultyAnswers(answers);  // Reloading
	}

	private synchronized List<FacultyAnswersAggregate> aggreateFacultyAnswers(List<Answer> answers) {
		ConcurrentMap<Faculty, FacultyAnswersAggregate> models = new ConcurrentHashMap<Faculty, FacultyAnswersAggregate>();
		for (Answer answer: answers) {
			if (!models.containsKey(answer.getFaculty())) {
				FacultyAnswersAggregate aggregate = new FacultyAnswersAggregate();
				aggregate.setFaculty(answer.getFaculty());
				aggregate.setAnswers(new HashSet<Answer>());
				models.put(answer.getFaculty(), aggregate);
			}
			FacultyAnswersAggregate aggregate = models.get(answer.getFaculty());
			aggregate.getAnswers().add(answer);
		}
		return Lists.newArrayList(models.values());
	}

}