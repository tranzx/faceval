package edu.iut.service;

import edu.iut.model.QuestionType;

/**
 * Business service interface to handle communication between web and
 * domain layer.
 *
 */
public interface QuestionTypeManager extends GenericManager<QuestionType, Long> {

}