package edu.iut.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;

import edu.iut.model.Answer;
import edu.iut.model.Classroom;
import edu.iut.model.Faculty;
import edu.iut.model.Student;

@Service("studentService")
public class StudentService implements InitializingBean {

	@Autowired private StudentManager studentManager;
	@Autowired private ClassroomManager classroomManager;
	@Autowired private EvaluationManager evaluationManager;
	@Autowired private AnswerManager answerManager;

	StudentFacultyIdentityMap identityMap;

	@Override
	public void afterPropertiesSet() throws Exception {
		identityMap = new StudentFacultyIdentityMap();
		// identityMap could be filled with all usernames here, for example.
	}

	public List<Faculty> getMyFaculties(String username) {
		if (identityMap.contains(username)) {
			return new ArrayList<Faculty>(identityMap.get(username));
		}
		Student student = studentManager.findByUsername(username);
		List<Classroom> classrooms = classroomManager.findByGroupClass(student.getGroupClass());
		List<Faculty> faculties = new ArrayList<Faculty>();
		Set<Faculty> electedForEvaluation = evaluationManager.findCurrentActiveEvaluation().getFaculties();
		if (classrooms != null) {
			for (Classroom cr : classrooms) {
				if (electedForEvaluation.contains(cr.getFaculty())) {
					faculties.add(cr.getFaculty());
				}
			}
		}
		identityMap.put(username, new HashSet<Faculty>(faculties));

		return getMyFaculties(username);
	}

	public List<Student> findNotAttendedStudents() {
		List<Answer> answers = answerManager.findAnswersForCurrentEvaluation();
		Set<Student> onTimeStudents = new HashSet<Student>();
		for (Answer answer: answers) {
			onTimeStudents.add(answer.getStudent());
		}
		List<Student> allStudents = studentManager.getAllDistinct();
		Set<Student> lazyStudents = new HashSet<Student>();
		for (Student student: allStudents) {
			if (!onTimeStudents.contains(student)) {
				lazyStudents.add(student);
			}
		}
		return Lists.newArrayList(lazyStudents);
	}

}