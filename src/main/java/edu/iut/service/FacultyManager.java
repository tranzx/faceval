package edu.iut.service;

import edu.iut.model.Faculty;

/**
 * Business service interface to handle communication between web and
 * domain layer.
 *
 */
public interface FacultyManager extends GenericManager<Faculty, Long> {

}