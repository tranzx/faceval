package edu.iut.service;

import java.util.List;

import edu.iut.model.GroupEducation;

/**
 * Business service interface to handle communication between web and
 * domain layer.
 *
 */
public interface GroupEducationManager extends GenericManager<GroupEducation, Long> {

	List<GroupEducation> getGroupEducations();
}