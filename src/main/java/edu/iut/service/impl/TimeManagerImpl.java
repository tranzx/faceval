package edu.iut.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.iut.dao.TimeDao;
import edu.iut.model.Time;
import edu.iut.service.TimeManager;

@Service("timeManager")
public class TimeManagerImpl extends GenericManagerImpl<Time, Long> implements TimeManager {
	private TimeDao timeDao;

	@Autowired
	public void setTimeDao(TimeDao timeDao) {
		this.dao=timeDao;
		this.timeDao = timeDao;
	}
}
