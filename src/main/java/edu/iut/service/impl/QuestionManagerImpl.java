package edu.iut.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.iut.dao.QuestionDao;
import edu.iut.model.Question;
import edu.iut.service.QuestionManager;

@Service("questionManager")
public class QuestionManagerImpl extends GenericManagerImpl<Question, Long> implements QuestionManager {
	private QuestionDao questionDao;

	@Autowired
	public void setQuestionDao(QuestionDao questionDao) {
		this.dao = questionDao;
		this.questionDao = questionDao;
	}

}