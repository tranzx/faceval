package edu.iut.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.iut.dao.EvaluationDao;
import edu.iut.model.Evaluation;
import edu.iut.model.Semester;
import edu.iut.service.EvaluationManager;

@Service("evaluationManager")
public class EvaluationManagerImpl extends GenericManagerImpl<Evaluation, Long> implements EvaluationManager {

	private EvaluationDao evaluationDao;

	@Autowired
	public void setEvaluationDao(EvaluationDao evaluationDao) {
		this.dao = evaluationDao;
		this.evaluationDao = evaluationDao;
	}

	@Override
	public List<Evaluation> getEvlauations() {
		return evaluationDao.getAllDistinct();
	}

	@Override
	public Evaluation findByYearAndSemester(String year, Semester semester) {
		return evaluationDao.findByYearAndSemester(year, semester);
	}

	@Override
	public Evaluation findCurrentActiveEvaluation() {
		return evaluationDao.findCurrentActiveEvaluation();
	}

}