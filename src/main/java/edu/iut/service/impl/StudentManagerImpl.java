package edu.iut.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.iut.dao.StudentDao;
import edu.iut.model.Student;
import edu.iut.model.User;
import edu.iut.service.StudentManager;
import edu.iut.service.UserManager;

@Service("studentManager")
public class StudentManagerImpl extends GenericManagerImpl<Student, Long> implements StudentManager {
	private StudentDao studentDao;
	@Autowired UserManager userManager;

	@Autowired
	public void setStudentDao(StudentDao studentDao) {
		this.dao=studentDao;
		this.studentDao = studentDao;
	}

	@Override
	public Student findByUsername(String username) {
		User user = userManager.getUserByUsername(username);
		return studentDao.findByUser(user);
	}

	@Override
	public List<Student> getAllDistinct() {
		return studentDao.getAllDistinct();
	}

}