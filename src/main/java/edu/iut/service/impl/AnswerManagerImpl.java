package edu.iut.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.iut.dao.AnswerDao;
import edu.iut.model.Answer;
import edu.iut.model.Evaluation;
import edu.iut.service.AnswerManager;
import edu.iut.service.EvaluationManager;

@Service("answerManager")
public class AnswerManagerImpl extends GenericManagerImpl<Answer, Long> implements AnswerManager {

	private AnswerDao answerDao;
	@Autowired private EvaluationManager evaluationManager;

	@Autowired
	public void setAnswerDao(AnswerDao answerDao) {
		this.answerDao = answerDao;
		this.dao = answerDao;
	}

	@Override
	public List<Answer> getAllDistinct() {
		return answerDao.getAllDistinct();
	}

	@Override
	public List<Answer> findByEvaluation(Evaluation evaluation) {
		return answerDao.findByEvaluation(evaluation);
	}

	@Override
	public List<Answer> findAnswersForCurrentEvaluation() {
		Evaluation evaluation = evaluationManager.findCurrentActiveEvaluation();
		return findByEvaluation(evaluation);
	}

}