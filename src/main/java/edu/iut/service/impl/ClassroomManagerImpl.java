package edu.iut.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.iut.dao.ClassroomDao;
import edu.iut.model.Classroom;
import edu.iut.model.GroupClass;
import edu.iut.service.ClassroomManager;

@Service("classroomManager")
public class ClassroomManagerImpl extends GenericManagerImpl<Classroom, Long> implements ClassroomManager {
	private ClassroomDao classRoomDao;

	@Autowired
	public void setClassRoomDao(ClassroomDao classRoomDao) {
		this.classRoomDao = classRoomDao;
		this.dao=classRoomDao;
	}

	@Override
	public List<Classroom> findByGroupClass(GroupClass groupClass) {
		return classRoomDao.findByGroupClass(groupClass);
	}
}
