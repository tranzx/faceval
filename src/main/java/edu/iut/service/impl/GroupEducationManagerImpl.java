package edu.iut.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.iut.dao.GroupEducationDao;
import edu.iut.model.GroupEducation;
import edu.iut.service.GroupEducationManager;

@Service("groupEducationManager")
public class GroupEducationManagerImpl extends GenericManagerImpl<GroupEducation, Long> implements GroupEducationManager {
	private GroupEducationDao groupEducationDao;

	@Autowired
	public void setGroupEducationDao(GroupEducationDao groupEducationDao) {
		this.dao=groupEducationDao;
		this.groupEducationDao = groupEducationDao;
	}

	@Override
	public List<GroupEducation> getGroupEducations() {
		return groupEducationDao.getAllDistinct();
	}
}
