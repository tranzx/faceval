package edu.iut.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.iut.dao.FacultyDao;
import edu.iut.model.Faculty;
import edu.iut.service.FacultyManager;

@Service("facultyManager")
public class FacultyManagerImpl extends GenericManagerImpl<Faculty, Long> implements FacultyManager {
	private FacultyDao facultyDao;

	
	@Autowired
	public void setFacultyDao(FacultyDao facultyDao) {
		this.dao=facultyDao;
		this.facultyDao = facultyDao;
	}
	
}
