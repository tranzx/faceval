package edu.iut.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.iut.dao.QuestionTypeDao;
import edu.iut.model.QuestionType;
import edu.iut.service.QuestionTypeManager;

@Service("questionTypeManager")
public class QuestionTypeManagerImpl extends GenericManagerImpl<QuestionType, Long> implements QuestionTypeManager {
	private QuestionTypeDao questionTypeDao;

	@Autowired
	public void setGroupEducationDao(QuestionTypeDao questionTypeDao) {
		this.dao=questionTypeDao;
		this.questionTypeDao = questionTypeDao;
	}

}
