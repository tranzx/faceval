package edu.iut.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.iut.dao.GroupClassDao;
import edu.iut.model.GroupClass;
import edu.iut.service.GroupClassManager;

@Service("groupClassManager")
public class GroupClassManagerImpl extends GenericManagerImpl<GroupClass, Long> implements GroupClassManager {
	private GroupClassDao groupClassDao;

	@Autowired
	public void setGroupClassDao(GroupClassDao groupClassDao) {
		this.dao=groupClassDao;
		this.groupClassDao = groupClassDao;
	}

	@Override
	public List<GroupClass> getGroupClasses() {
		return groupClassDao.getAllDistinct();
	}
}
