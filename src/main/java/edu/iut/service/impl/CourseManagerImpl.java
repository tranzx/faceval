package edu.iut.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.iut.dao.CourseDao;
import edu.iut.model.Course;
import edu.iut.service.CourseManager;

@Service("courseManager")
public class CourseManagerImpl extends GenericManagerImpl<Course, Long> implements CourseManager {
	private CourseDao courseDao;

	@Autowired
	public void setCourseDao(CourseDao courseDao) {
		this.dao = courseDao;
		this.courseDao = courseDao;
	}
}
