package edu.iut.service;

import java.util.List;

import edu.iut.model.Student;

/**
 * Business service interface to handle communication between web and
 * domain layer.
 *
 */
public interface StudentManager extends GenericManager<Student, Long> {

	Student findByUsername(String username);

	List<Student> getAllDistinct();

}