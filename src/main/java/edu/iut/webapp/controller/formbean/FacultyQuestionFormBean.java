package edu.iut.webapp.controller.formbean;

import edu.iut.model.Faculty;
import edu.iut.model.Question;

public class FacultyQuestionFormBean {

	private Faculty faculty;
	private Question question;

	public Faculty getFaculty() {
		return faculty;
	}

	public Question getQuestion() {
		return question;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

}