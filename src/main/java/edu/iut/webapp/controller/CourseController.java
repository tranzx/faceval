package edu.iut.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.iut.model.Course;
import edu.iut.service.CourseManager;

/**
 * A Controller for Course entity.
 */
@Controller
@RequestMapping("/admin")
public class CourseController extends BaseFormController {
	@Autowired
	private CourseManager courseManager;

	@RequestMapping(value = "/courses", method = RequestMethod.GET)
	public String listCourse(Model model) throws Exception {
		model.addAttribute("courseList", courseManager.getAll());
		return "/admin/courseList";
	}

	@RequestMapping(value = "/courseform", method = RequestMethod.GET)
	public String showForm(Model model) throws Exception {
		model.addAttribute("course", new Course());
		return "/admin/courseForm";
	}

	@RequestMapping(value = "/courseform", method = RequestMethod.POST)
	public String submitForm(@Validated Course course, BindingResult errors) {
		validator.validate(course, errors);
		if (errors.hasErrors()) {
			return "/admin/courseForm";
		}
		courseManager.save(course);
		return "redirect:/admin/courses";
	}
	
	@RequestMapping(value ="/course/{id}", method = RequestMethod.POST)
	public String deleteCourse(@PathVariable("id") Long id) {
		courseManager.remove(id);
		return "redirect:/admin/courses";
	}
	
}
