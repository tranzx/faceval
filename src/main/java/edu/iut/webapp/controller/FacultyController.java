package edu.iut.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import edu.iut.model.Faculty;
import edu.iut.service.FacultyManager;

/**
 * A Controller for Faculty entity.
 */
@Controller
@RequestMapping("/admin")
public class FacultyController extends BaseFormController {
	@Autowired
	private FacultyManager facultyManager;

	@RequestMapping(value = "/faculties", method = RequestMethod.GET)
	public String listFaculty(Model model) throws Exception {
		model.addAttribute("facultyList", facultyManager.getAll());
		return "/admin/facultyList";
	}

	@RequestMapping(value = "/facultyform", method = RequestMethod.GET)
	public String showForm(Model model) throws Exception {
		model.addAttribute("faculty", new Faculty());
		return "/admin/facultyForm";
	}

	@RequestMapping(value = "/facultyform", method = RequestMethod.POST)
	public String submitForm(@Validated Faculty faculty, BindingResult errors) {
		validator.validate(faculty, errors);
		if (errors.hasErrors()) {
			return "/admin/facultyForm";
		}
		facultyManager.save(faculty);
		return "redirect:/admin/faculties";
	}
	
	@RequestMapping(value ="/faculty/{id}", method = RequestMethod.POST)
	public String deleteFaculty(@PathVariable("id") Long id) {
		facultyManager.remove(id);
		return "redirect:/admin/faculties";
	}
}
