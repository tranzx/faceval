package edu.iut.webapp.controller;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.iut.service.UserManager;

@Controller
@RequestMapping("/admin/users/report")
public class UserReportController {

	@Autowired UserManager userManager;

	@RequestMapping("/list/{format}")
	public String reportUserList(ModelMap modelMap, @PathVariable String format) {
		JRBeanCollectionDataSource jrDataSource = new JRBeanCollectionDataSource(userManager.getUsers());
		modelMap.put("reportData", jrDataSource );
		modelMap.put("format", format);
		return "userListReport";
	}

}