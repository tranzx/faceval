package edu.iut.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.iut.model.GroupClass;
import edu.iut.service.GroupClassManager;
import edu.iut.service.GroupEducationManager;
import edu.iut.service.StudentManager;

/**
 * A Controller for GroupClass entity.
 */
@Controller
@RequestMapping("/admin")
public class GroupClassController extends BaseFormController {

	@Autowired private GroupClassManager groupClassManager;
	@Autowired private GroupEducationManager groupEducationManager;

	@RequestMapping(value = "/groupClasses", method = RequestMethod.GET)
	public String listGroupClass(Model model) throws Exception {
		model.addAttribute("groupClassList", groupClassManager.getAll());
		return "/admin/groupClassList";
	}

	@RequestMapping(value = "/groupClassform", method = RequestMethod.GET)
	public String showForm(Model model) throws Exception {
		model.addAttribute("groupClass", new GroupClass());
		model.addAttribute("groupEducationItem", groupEducationManager.getAll());
		return "/admin/groupClassForm";
	}

	@RequestMapping(value = "/groupClassform", method = RequestMethod.POST)
	public String submitForm(@Validated GroupClass groupClass, BindingResult errors, Model model) {
		validator.validate(groupClass, errors);
		if (errors.hasErrors()) {
			model.addAttribute("groupEducationItem", groupEducationManager.getAll());
			return "/admin/groupClassForm";
		}
		groupClassManager.save(groupClass);
		return "redirect:/admin/groupClasses";
	}

	@RequestMapping(value ="/groupClass/{id}", method = RequestMethod.POST)
	public String deleteGroupClass(@PathVariable("id") Long id) {
		groupClassManager.remove(id);
		return "redirect:/admin/groupClasses";
	}
}