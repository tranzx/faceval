package edu.iut.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.iut.service.AnswerManager;

@Controller
@RequestMapping("/admin")
public class AnswerController extends BaseFormController {

	@Autowired AnswerManager answerManager;

	@RequestMapping(value = "/answers", method = RequestMethod.GET)
	public String listAnswer(Model model) {
		model.addAttribute("answerList", answerManager.findAnswersForCurrentEvaluation());
		return "/admin/answerList";
	}

	@RequestMapping(value = "/answer/{id}", method = RequestMethod.POST)
	public String deleteAnswer(@PathVariable("id") Long id) {
		answerManager.remove(id);
		return "redirect:/admin/answers";
	}

}