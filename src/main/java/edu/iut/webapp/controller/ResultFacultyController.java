package edu.iut.webapp.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.iut.service.EvaluationManager;
import edu.iut.service.FacultyService;
import edu.iut.service.dto.FacultyAnswersAggregate;
import edu.iut.webapp.controller.formbean.FacultyQuestionFormBean;

@Controller
@RequestMapping("/admin")
public class ResultFacultyController {

	@Autowired private FacultyService facultyService;
	@Autowired private EvaluationManager evaluationManager;

	@RequestMapping(value = "/resultfaculty")
	public String displayResult(Model model) {
		fillModelAttributes(model, facultyService.currentFacultyAnswersAggregate());
		return "/admin/resultFacultyList";
	}

	@RequestMapping(value = "/resultfaculty/reload")
	public String reloadResult(Model model) {
		facultyService.reloadFacultyAnswerAggregate();
		fillModelAttributes(model, facultyService.currentFacultyAnswersAggregate());
		return "/admin/resultFacultyList";
	}

	@RequestMapping(value = "/resultfaculty/filter", method = RequestMethod.POST)
	public String filterResult(Model model, FacultyQuestionFormBean formBean) {
		List<FacultyAnswersAggregate> aggregates = facultyService.currentFacultyAnswersAggregate();
		List<FacultyAnswersAggregate> filterred = new ArrayList<FacultyAnswersAggregate>();

		if (formBean.getFaculty() != null && formBean.getQuestion() != null) {
			filterred = FacultyAnswersAggregate.filterWithFacultyAndQuestion(aggregates, formBean.getFaculty(), formBean.getQuestion());
		} else if (formBean.getFaculty() != null && formBean.getQuestion() == null) {
			filterred = FacultyAnswersAggregate.filterWithFaculty(aggregates, formBean.getFaculty());
		} else if (formBean.getFaculty() == null && formBean.getQuestion() != null) {
			filterred = FacultyAnswersAggregate.filterWithQuestion(aggregates, formBean.getQuestion());
		} else {
			filterred = aggregates; // No filtering is necessary
		}

		fillModelAttributes(model, filterred);
		model.addAttribute("facultyQuestionFormBean", formBean);
		return "/admin/resultFacultyList";
	}

	@RequestMapping(value = "/resultfaculty/filter", method = RequestMethod.GET)
	public String filterResultGet() {
		return "redirect:/admin/resultfaculty"; // Simply redirect GET requests to displayResult method.
	}

	private void fillModelAttributes(Model model, List<FacultyAnswersAggregate> list) {
		model.addAttribute("faaList", list);
		model.addAttribute("faculties", evaluationManager.findCurrentActiveEvaluation().getFaculties());
		model.addAttribute("questions", evaluationManager.findCurrentActiveEvaluation().getQuestions());
		model.addAttribute("facultyQuestionFormBean", new FacultyQuestionFormBean());
	}

}