package edu.iut.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.iut.model.Student;
import edu.iut.model.Time;
import edu.iut.service.StudentManager;
import edu.iut.service.TimeManager;

/**
 * A Controller for Student entity.
 */
@Controller
@RequestMapping("/admin")
public class TimeController extends BaseFormController {
	@Autowired
	private TimeManager timeManager;

	@RequestMapping(value = "/times", method = RequestMethod.GET)
	public String listTime(Model model) throws Exception {
		model.addAttribute("timeList", timeManager.getAll());
		return "/admin/timeList";
	}

	@RequestMapping(value = "/timeform", method = RequestMethod.GET)
	public String showForm(Model model) throws Exception {
		model.addAttribute("time", new Time());
		return "/admin/timeForm";
	}

	@RequestMapping(value = "/timeform", method = RequestMethod.POST)
	public String submitForm(@Validated Time time, BindingResult errors) {
		validator.validate(time, errors);
		if (errors.hasErrors()) {
			return "/admin/timeForm";
		}
		timeManager.save(time);
		return "redirect:/admin/times";
	}
}
