package edu.iut.webapp.controller;

import java.security.Principal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import edu.iut.model.Answer;
import edu.iut.model.Evaluation;
import edu.iut.model.Faculty;
import edu.iut.model.Level;
import edu.iut.model.Question;
import edu.iut.service.AnswerManager;
import edu.iut.service.EvaluationManager;
import edu.iut.service.FacultyManager;
import edu.iut.service.StudentManager;
import edu.iut.service.StudentService;

@Controller
@RequestMapping("/student")
public class MyFacultyController extends BaseFormController {

	@Autowired
	private StudentService studentService;
	@Autowired
	private StudentManager studentManager;
	@Autowired
	private FacultyManager facultyManager;
	@Autowired
	private CookieLocaleResolver localeResolver;
	@Autowired
	private AnswerManager answerManager;
	@Autowired
	private EvaluationManager evaluationManager;

	private Evaluation eval = null;

	@RequestMapping(value = "/myfaculties", method = RequestMethod.GET)
	public String listMyFaculties(Model model, Principal principal) {
		List<Faculty> studentFaculties = studentService.getMyFaculties(principal.getName());
		Set<Faculty> evaluationFaculties = getEval().getFaculties();
		Set<Faculty> selectedFaculties = new HashSet<Faculty>();
		for (Faculty stufac : studentFaculties) {
			for (Faculty evalfac : evaluationFaculties) {
				if (stufac.getId().equals(evalfac.getId())) {
					selectedFaculties.add(stufac);
				}
			}
		}
		model.addAttribute("facultyList", selectedFaculties);
		return "/student/myFaculties";
	}

	@RequestMapping(value = "/enter")
	public String enterToEvaluation(Model model, @PathParam("id") Long id) {
		model.addAttribute("questionList", getEval().getQuestions());
		model.addAttribute("selectedFaculty", facultyManager.get(id));
		return "/student/enterToEvaluation";
	}

	@RequestMapping(value = "/submit/{facultyId}", method = RequestMethod.POST)
	public String submitEvaluation(@PathVariable("facultyId") Long facultyId,
			HttpServletRequest request, Principal principal) {
		Set<Question> allQ = getEval().getQuestions();
		boolean errorHappenWhileSaving = false;
		for (Question question : allQ) {
			Answer answer = new Answer();
			String answerString = request.getParameter("answer"+ question.getId());
			if (answerString == null || answerString.isEmpty()) {
				log.info(String.format("Question with id [%s] doesn't has any answer provided",
								question.getId()));
				answer.setLevel(Level.EMPTY);
			} else {
				answer.setLevel(Level.valueOf(answerString));
			}
			answer.setQuestion(question);
			answer.setFaculty(facultyManager.get(facultyId));
			answer.setStudent(studentManager.findByUsername(principal.getName()));
			answer.setEvaluation(getEval());
			try {
				answerManager.save(answer);
			} catch (DataAccessException dae) {
				errorHappenWhileSaving = true;
			}
		}
		if (errorHappenWhileSaving) {
			saveError(request, getText("evaluation.dataErrorMessage", localeResolver.resolveLocale(request)));
		} else {
			saveMessage(request, getText("evaluation.successMessage", localeResolver.resolveLocale(request)));
		}
		return "redirect:/student/myfaculties";
	}

	public Evaluation getEval() {
//		if (eval == null) {
//			eval = evaluationManager.findCurrentActiveEvaluation();
//		}
		return evaluationManager.findCurrentActiveEvaluation();
	}

}