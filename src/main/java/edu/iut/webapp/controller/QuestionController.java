package edu.iut.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.iut.model.Question;
import edu.iut.service.QuestionManager;
import edu.iut.service.QuestionTypeManager;

/**
 *
 * A Controller for Question entity.
 */
@Controller
@RequestMapping("/admin")
public class QuestionController extends BaseFormController {
	@Autowired QuestionManager questionManager;
	@Autowired QuestionTypeManager questionTypeManager;

	@RequestMapping(value = "questions", method = RequestMethod.GET)
	public String listQuestion(Model model) throws Exception {
		model.addAttribute("questionList", questionManager.getAll());
		return "/admin/questionList";
	}

	@RequestMapping(value = "/questionform", method = RequestMethod.POST)
	public String submitForm(@Validated Question question, BindingResult errors, Model model) {
		validator.validate(question, errors);
		if (errors.hasErrors()) {
			model.addAttribute("questionTypeItem", questionTypeManager.getAll());
			return "/admin/questionForm";
		}
		questionManager.save(question);
		return "redirect:/admin/questions";
	}

	@RequestMapping(value = "/questionform", method = RequestMethod.GET)
	public String showForm(Model model) throws Exception {
		model.addAttribute("question", new Question());
		model.addAttribute("questionTypeItem", questionTypeManager.getAll());
		return "/admin/questionForm";
	}

	@RequestMapping(value ="/question/{id}", method = RequestMethod.POST)
	public String deleteQuestion(@PathVariable("id") Long id) {
		questionManager.remove(id);
		return "redirect:/admin/questions";
	}
}