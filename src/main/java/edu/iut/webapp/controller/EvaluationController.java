package edu.iut.webapp.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import edu.iut.model.Evaluation;
import edu.iut.model.Semester;
import edu.iut.service.EvaluationManager;
import edu.iut.service.FacultyManager;
import edu.iut.service.QuestionManager;

/**
 * Simple class to retrieve a list of evaluations from the database.
 *
 */
@Controller
@RequestMapping("/admin")
public class EvaluationController extends BaseFormController {
	@Autowired
	private EvaluationManager evaluationManager;
	@Autowired
	private QuestionManager questionManager;
	@Autowired
	private FacultyManager facultyManager;
	
	@RequestMapping(value="/evaluations", method = RequestMethod.GET)
	public ModelAndView showEvaluationList(Model model) throws Exception {
		model.addAttribute("evaluationList", evaluationManager.getEvlauations());
		return new ModelAndView("/admin/evaluationList", model.asMap());
	}

	@RequestMapping(value="/evaluationform", method = RequestMethod.POST)
	public String onSubmitEvaluationForm(Model model, Evaluation evaluation,
			BindingResult errors, HttpServletRequest request) throws Exception {
		validator.validate(evaluation, errors);
		if (errors.hasErrors()) {
			model.addAttribute("semesters",Semester.values());
			model.addAttribute("questionSet", questionManager.getAll());
			model.addAttribute("facultySet", facultyManager.getAll());
			return "/admin/evaluationForm";
		}
		evaluation.setCreatedAt(new Date());
		String[] evalQuestions = request.getParameterValues("evalQuestions");
		evaluation.getQuestions().clear(); // empty questions
		if (evalQuestions != null) {
			for (String questionId : evalQuestions) {
				evaluation.addQuestion(questionManager.get(new Long(questionId)));
			}
		}
		String[] evalFaculties = request.getParameterValues("evalFaculties");
		evaluation.getFaculties().clear(); // empty faculties
		if (evalFaculties != null) {
			for (String facultyId : evalFaculties) {
				evaluation.addFaculty(facultyManager.get(new Long(facultyId)));
			}
		}
		evaluationManager.save(evaluation);

		return "redirect:/admin/evaluations";
	}

	@RequestMapping(value ="/evaluation", method = RequestMethod.POST)
	public String deleteEvaluation(@RequestParam("id") Long id) {
		evaluationManager.remove(id);
		return "redirect:/admin/evaluations";
	}

	@RequestMapping(value="/evaluationform", method = RequestMethod.GET)
	public ModelAndView showEvaluationForm(Model model) {
		model.addAttribute("evaluation", new Evaluation());
		model.addAttribute("questionSet", questionManager.getAll());
		model.addAttribute("facultySet", facultyManager.getAll());
		model.addAttribute("semesters",Semester.values());
		return new ModelAndView("/admin/evaluationForm", model.asMap());
	}

	@RequestMapping(value="/evaluationEditform", method = RequestMethod.GET)
	public String showEvaluationUpdateForm(@RequestParam("id") Long id, Model model) {
		model.addAttribute("evaluation", evaluationManager.get(id));
		model.addAttribute("questionSet", questionManager.getAll());
		model.addAttribute("facultySet", facultyManager.getAll());
		model.addAttribute("semesters",Semester.values());
		return "/admin/evaluationForm";
	}

}