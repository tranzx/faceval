package edu.iut.webapp.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import edu.iut.model.Classroom;
import edu.iut.service.ClassroomManager;
import edu.iut.service.CourseManager;
import edu.iut.service.FacultyManager;
import edu.iut.service.GroupClassManager;

/**
 * A Controller for ClassRoom entity.
 */
@Controller
@RequestMapping("/admin")
public class ClassroomController extends BaseFormController {

	@Autowired private ClassroomManager classroomManager;
	@Autowired private GroupClassManager groupClassManager;
	@Autowired private FacultyManager facultyManager;
	@Autowired private CourseManager courseManager;
	@Autowired private CookieLocaleResolver localeResolver;

	@RequestMapping(value = "/classrooms", method = RequestMethod.GET)
	public String listClassroom(Model model) throws Exception {
		model.addAttribute("classroomList", classroomManager.getAll());
		return "/admin/classroomList";
	}

	@RequestMapping(value = "/classroomform", method = RequestMethod.GET)
	public String showForm(Model model) throws Exception {
		model.addAttribute("classroom", new Classroom());
		model.addAttribute("groupClassItem", groupClassManager.getGroupClasses());
		model.addAttribute("facultyItem", facultyManager.getAll());
		model.addAttribute("courseItem", courseManager.getAll());
		return "/admin/classroomForm";
	}

	@RequestMapping(value = "/classroomform", method = RequestMethod.POST)
	public String submitForm(Model model, Classroom classroom, BindingResult errors
			, HttpServletRequest request) throws Exception {

		validator.validate(classroom, errors);
		if (errors.hasErrors()) {
			model.addAttribute("groupClassItem",groupClassManager.getGroupClasses());
			model.addAttribute("facultyItem", facultyManager.getAll());
			model.addAttribute("courseItem", courseManager.getAll());
			return "/admin/classroomForm";
		}

		try {
			classroomManager.save(classroom);
			saveMessage(request, getText("classroom.successMessage", localeResolver.resolveLocale(request)));
		} catch (DataAccessException dae) {
		//	errorHappenWhileSaving = true;
			saveError(request, getText("classroom.dataErrorMessage", localeResolver.resolveLocale(request)));
		}
//	}
//	if (errorHappenWhileSaving) {
//		saveError(request, getText("evaluation.dataErrorMessage", localeResolver.resolveLocale(request)));
//	} else {
//		saveMessage(request, getText("evaluation.successMessage", localeResolver.resolveLocale(request)));
//	}
		
		
		
		return "redirect:/admin/classrooms";
	}

	@RequestMapping(value ="/classroom/{id}", method = RequestMethod.POST)
	public String deleteClassroom(@PathVariable("id") Long id) {
		classroomManager.remove(id);
		return "redirect:/admin/classrooms";
	}
}