package edu.iut.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.iut.model.GroupEducation;
import edu.iut.service.GroupEducationManager;

/**
 * A Controller for GroupEducation entity.
 */
@Controller
@RequestMapping("/admin")
public class GroupEducationController extends BaseFormController {

	@Autowired private GroupEducationManager groupEducationManager;

	@RequestMapping(value = "/groupEducations", method = RequestMethod.GET)
	public String listGroupEducation(Model model) throws Exception {
		model.addAttribute("groupEducationList", groupEducationManager.getAll());
		return "/admin/groupEducationList";
	}

	@RequestMapping(value = "/groupEducationform", method = RequestMethod.GET)
	public String showForm(Model model) throws Exception {
		model.addAttribute("groupEducation", new GroupEducation());
		return "/admin/groupEducationForm";
	}

	@RequestMapping(value = "/groupEducationform", method = RequestMethod.POST)
	public String submitForm(@Validated GroupEducation groupEducation, BindingResult errors, Model model) {
		validator.validate(groupEducation, errors);
		if (errors.hasErrors()) {
			return "/admin/groupEducationForm";
		}
		groupEducationManager.save(groupEducation);
		return "redirect:/admin/groupEducations";
	}

	@RequestMapping(value ="/groupEducation/{id}", method = RequestMethod.POST)
	public String deleteGroupEducation(@PathVariable("id") Long id) {
		groupEducationManager.remove(id);
		return "redirect:/admin/groupEducations";
	}
}