package edu.iut.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.iut.model.GroupEducation;
import edu.iut.model.QuestionType;
import edu.iut.service.GroupEducationManager;
import edu.iut.service.QuestionTypeManager;

/**
 * A Controller for QuestionType entity.
 */
@Controller
@RequestMapping("/admin")
public class QuestionTypeController extends BaseFormController {

	@Autowired private QuestionTypeManager questionTypeManager;

	@RequestMapping(value = "/questionTypes", method = RequestMethod.GET)
	public String listGroupEducation(Model model) throws Exception {
		model.addAttribute("questionTypeList", questionTypeManager.getAll());
		return "/admin/questionTypeList";
	}

	@RequestMapping(value = "/questionTypeform", method = RequestMethod.GET)
	public String showForm(Model model) throws Exception {
		model.addAttribute("questionType", new QuestionType());
		return "/admin/questionTypeForm";
	}

	@RequestMapping(value = "/questionTypeform", method = RequestMethod.POST)
	public String submitForm(@Validated QuestionType questionType, BindingResult errors, Model model) {
		validator.validate(questionType, errors);
		if (errors.hasErrors()) {
			return "/admin/questionTypeForm";
		}
		questionTypeManager.save(questionType);
		return "redirect:/admin/questionTypes";
	}

	@RequestMapping(value ="/questionType/{id}", method = RequestMethod.POST)
	public String deleteQuestionType(@PathVariable("id") Long id) {
		questionTypeManager.remove(id);
		return "redirect:/admin/questionTypes";
	}
}