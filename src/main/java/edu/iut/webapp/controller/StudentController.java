package edu.iut.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import edu.iut.model.Student;
import edu.iut.service.StudentManager;

/**
 * A Controller for Student entity.
 */
@Controller
@RequestMapping("/admin")
public class StudentController extends BaseFormController {
	@Autowired
	private StudentManager studentManager;

	@RequestMapping(value = "/students", method = RequestMethod.GET)
	public String listStudent(Model model) throws Exception {
		model.addAttribute("studentList", studentManager.getAllDistinct());
		return "/admin/studentList";
	}

	@RequestMapping(value = "/studentform", method = RequestMethod.GET)
	public String showForm(Model model) throws Exception {
		model.addAttribute("student", new Student());
		return "/admin/studentForm";
	}

	@RequestMapping(value = "/studentform", method = RequestMethod.POST)
	public String submitForm(@Validated Student student, BindingResult errors) {
		validator.validate(student, errors);
		if (errors.hasErrors()) {
			return "/admin/studentForm";
		}
		studentManager.save(student);
		return "redirect:/admin/students";
	}
	
	@RequestMapping(value ="/student/{id}", method = RequestMethod.POST)
	public String deleteStudent(@PathVariable("id") Long id) {
		studentManager.remove(id);
		return "redirect:/admin/students";
	}

}