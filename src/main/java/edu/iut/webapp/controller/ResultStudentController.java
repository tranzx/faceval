package edu.iut.webapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import edu.iut.service.StudentService;

@Controller
@RequestMapping("/admin")
public class ResultStudentController {

	@Autowired StudentService studentService;

	@RequestMapping("/resultstudent")
	public String displayResult(Model model) {
		model.addAttribute("resultStudentList", studentService.findNotAttendedStudents());
		return "/admin/resultStudentList";
	}

}