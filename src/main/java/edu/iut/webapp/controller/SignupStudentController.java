package edu.iut.webapp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.cxf.interceptor.security.AccessDeniedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

import edu.iut.Constants;
import edu.iut.model.Student;
import edu.iut.model.User;
import edu.iut.service.GroupClassManager;
import edu.iut.service.RoleManager;
import edu.iut.service.StudentManager;
import edu.iut.service.UserExistsException;

/**
 * Controller to signup new students.
 */
@Controller
@RequestMapping("/signup/student*")
public class SignupStudentController extends BaseFormController {

	@Autowired private RoleManager roleManager;
	@Autowired private StudentManager studentManager;
	@Autowired private CookieLocaleResolver localeResolver;
	@Autowired private GroupClassManager groupClassManager;

	public SignupStudentController() {
		setCancelView("redirect:/login");
		setSuccessView("redirect:/mainMenu");
	}

	@RequestMapping(method = RequestMethod.GET)
	public String showForm(Model model) {
		User user = new User();
		user.getAddress().setCountry("IR"); // default country
		Student student = new Student();
		student.setUser(user);
		model.addAttribute("student", student);
		model.addAttribute("groupClassList", groupClassManager.getAll());
		return "signup/student";
	}

	@RequestMapping(method = RequestMethod.POST)
	public String onSubmit(Student student, BindingResult errors, HttpServletRequest request, HttpServletResponse response, Model model)
			throws Exception {
		if (request.getParameter("cancel") != null) {
			return getCancelView();
		}

		validator.validate(student, errors);

		if (errors.hasErrors()) {
			model.addAttribute("groupClassList", groupClassManager.getAll());
			return "/signup/student";
		}

		student.getUser().setEnabled(true);

		// Set the default user role on this new student
		student.getUser().addRole(roleManager.getRole(Constants.USER_ROLE));
		student.getUser().addRole(roleManager.getRole(Constants.STUDENT_ROLE));

		try {
			User savedUser = this.getUserManager().saveUser(student.getUser());
			student.setUser(savedUser);
			studentManager.save(student);
		} catch (AccessDeniedException ade) {
			// thrown by UserSecurityAdvice configured in aop:advisor userManagerSecurity
			log.warn(ade.getMessage());
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
			return null;
		} catch (UserExistsException e) {
			errors.rejectValue("username", "errors.existing.user",
				new Object[] { student.getUser().getUsername(), student.getUser().getEmail()}, "duplicate user");

			// redisplay the unencrypted passwords
			student.getUser().setPassword(student.getUser().getConfirmPassword());
			return "/signup/student";
		}

		saveMessage(request, getText("user.registered", localeResolver.resolveLocale(request)));
		request.getSession().setAttribute(Constants.REGISTERED, Boolean.TRUE);

		// log user in automatically
		UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(
				student.getUser().getUsername(), student.getUser().getConfirmPassword(), student.getUser().getAuthorities());
		auth.setDetails(student.getUser());
		SecurityContextHolder.getContext().setAuthentication(auth);

		return getSuccessView();
	}

}