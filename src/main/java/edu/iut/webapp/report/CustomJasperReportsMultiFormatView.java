package edu.iut.webapp.report;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JasperPrint;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.view.jasperreports.JasperReportsMultiFormatView;


public class CustomJasperReportsMultiFormatView extends JasperReportsMultiFormatView {

	private final Log log = LogFactory.getLog(CustomJasperReportsMultiFormatView.class);
	@Override
	protected void renderReport(JasperPrint populatedReport, Map<String, Object> model, HttpServletResponse response) throws Exception {
		setReportDataKey("reportData");

		String format = model.get("format").toString();
		String filename = populatedReport.getName() + "." + format;

		response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");
		log.warn(" Report Encoding::::: '" + response.getCharacterEncoding());
		log.warn(" Report Locale::::: '" + response.getLocale());
		
		// PDF, CSV, HTML, XLS
		super.renderReport(populatedReport, model, response);
	}
}
