package edu.iut.webapp.report;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Iterator;

import javax.servlet.jsp.JspException;

import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.displaytag.Messages;
import org.displaytag.exception.BaseNestableJspTagException;
import org.displaytag.exception.DecoratorException;
import org.displaytag.exception.ObjectLookupException;
import org.displaytag.exception.SeverityEnum;
import org.displaytag.export.BinaryExportView;
import org.displaytag.model.Column;
import org.displaytag.model.ColumnIterator;
import org.displaytag.model.HeaderCell;
import org.displaytag.model.Row;
import org.displaytag.model.RowIterator;
import org.displaytag.model.TableModel;
import org.displaytag.util.TagConstants;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Cell;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfWriter;

/**
 * PDF exporter using IText. This class is provided to use instead of default
 * org.displaytag.export.PdfView class
 *
 */
public class DisplayTagPdfView implements BinaryExportView {

	/**
	 * TableModel to render.
	 */
	private TableModel model;

	/**
	 * export full list?
	 */
	private boolean exportFull;

	/**
	 * include header in export?
	 */
	private boolean header;

	/**
	 * decorate export?
	 */
	private boolean decorated;

	/**
	 * This is the table, added as an Element to the PDF document. It contains
	 * all the data, needed to represent the visiable table into the PDF
	 */
	private Table tablePDF;

	/**
	 * The default font used in the document.
	 */
	private Font smallFont;

	/**
	 * @see org.displaytag.export.ExportView#setParameters(TableModel, boolean, boolean, boolean)
	 */
	@Override
	public void setParameters(TableModel tableModel, boolean exportFullList, boolean includeHeader,
			boolean decorateValues) {
		this.model = tableModel;
		this.exportFull = exportFullList;
		this.header = includeHeader;
		this.decorated = decorateValues;
	}

	/**
	 * Initialize the main info holder table.
	 * @throws IOException 
	 * @throws DocumentException 
	 */
	protected void initTable() throws DocumentException, IOException {
		tablePDF = new Table(this.model.getNumberOfColumns());
//		tablePDF.setDefaultVerticalAlignment(Element.ALIGN_TOP);
		tablePDF.setCellsFitPage(true);
		tablePDF.setWidth(100);

		tablePDF.setPadding(2);
		tablePDF.setSpacing(0);

		BaseFont farsiFont = BaseFont.createFont("/app/src/main/resources/fonts/nazlib.ttf", BaseFont.IDENTITY_H ,BaseFont.EMBEDDED);
		smallFont = new Font(farsiFont, 11f, Font.NORMAL);
	}

	/**
	 * @see org.displaytag.export.BaseExportView#getMimeType()
	 * @return "application/pdf";
	 */
	@Override
	public String getMimeType() {
		return "application/pdf";
	}

	/**
	 * @see org.displaytag.export.BinaryExportView#doExport(OutputStream)
	 */
	@Override
	public void doExport(OutputStream out) throws IOException, JspException {
		try {
			initTable();

			Document document = new Document(PageSize.A4.rotate(), 60, 60, 40, 40);
			document.addCreationDate();
			HeaderFooter footer = new HeaderFooter(new Phrase(TagConstants.EMPTY_STRING, smallFont), true);
			footer.setBorder(Rectangle.NO_BORDER);
			footer.setAlignment(Element.ALIGN_CENTER);

			PdfWriter pdfWriter = PdfWriter.getInstance(document, out);
			pdfWriter.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);

			// Fill the virtual PDF table with the necessary data
			document.open();
			generatePDFTable(pdfWriter);
			document.setFooter(footer);
			document.add(this.tablePDF);
			document.close();

		} catch (Exception e) {
			throw new PdfGenerationException(e);
		}
	}

	/**
	 * The overall PDF table generator.
	 * @throws JspException for errors during value retrieving from the table model
	 * @throws DocumentException 
	 */
	protected void generatePDFTable(PdfWriter pdfWriter) throws JspException, DocumentException {
		if (this.header)
			generateHeaders(pdfWriter);

		tablePDF.endHeaders();
		generateRows();
	}

	/**
	 * Generate all the row cells.
	 * @throws DecoratorException 
	 * @throws ObjectLookupException 
	 * @throws JspException for errors during value retrieving from the table model
	 * @throws BadElementException errors while generating content
	 */
	protected void generateRows() throws ObjectLookupException, DecoratorException, BadElementException {
		// get the correct iterator (full or partial list according to the exportFull field)
		RowIterator rowIterator = this.model.getRowIterator(this.exportFull);

		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();

			ColumnIterator columnIterator = row.getColumnIterator(this.model.getHeaderCellList());

			while (columnIterator.hasNext()) {
				Column column = columnIterator.nextColumn();

				Object value = column.getValue(this.decorated);

				Cell cell = getCell(ObjectUtils.toString(value));
				tablePDF.addCell(cell);
			}
		}
	}

	/**
	 * Generate the header cells, Which persist on every page of the PDF document.
	 * @throws DocumentException 
	 */
	protected void generateHeaders(PdfWriter pdfWriter) throws DocumentException {
		@SuppressWarnings("unchecked")
		Iterator<HeaderCell> iterator = this.model.getHeaderCellList().iterator();

		int llx = 140;	// lower left x
		int lly = 570;	// lower left y
		int urx = 770;	// upper right x
		int ury = 100;	// upper right y
		while (iterator.hasNext()) {
			HeaderCell headerCell = iterator.next();
			
			String columHeader = headerCell.getTitle();

			if (columHeader == null) {
				columHeader = StringUtils.capitalize(headerCell.getBeanPropertyName());
			}

			ColumnText columnText = new ColumnText(pdfWriter.getDirectContent());
			columnText.setSimpleColumn(llx, lly, urx, ury);
			columnText.addElement(new Chunk(StringUtils.trimToEmpty(columHeader), smallFont));
			columnText.setRunDirection(PdfWriter.RUN_DIRECTION_RTL);
			columnText.setAlignment(Element.ALIGN_RIGHT);
			columnText.go();
			llx = llx + 200;
		}
	}

	/**
	 * Returns a formatted cell for the given value.
	 * @param value cell value
	 * @return Cell
	 * @throws BadElementException errors while generating content
	 */
	private Cell getCell(String value) throws BadElementException {
		Cell cell = new Cell(new Chunk(StringUtils.trimToEmpty(value), smallFont));
		cell.setVerticalAlignment(Element.ALIGN_TOP);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setLeading(8);
		return cell;
	}

	/**
	 * Wraps IText-generated exceptions.
	 */
	static class PdfGenerationException extends BaseNestableJspTagException {
		private static final long serialVersionUID = 1L;

		public PdfGenerationException(Throwable cause) {
			super(DisplayTagPdfView.class, Messages.getString("PdfView.errorexporting"), cause);
		}

		@Override
		public SeverityEnum getSeverity() {
			return SeverityEnum.ERROR;
		}
	}

}