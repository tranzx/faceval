package edu.iut.webapp;

import java.util.Locale;

import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.convert.converter.Converter;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;

import edu.iut.model.Course;
import edu.iut.model.Faculty;
import edu.iut.model.GroupClass;
import edu.iut.model.GroupEducation;
import edu.iut.model.Level;
import edu.iut.model.Question;
import edu.iut.model.QuestionType;
import edu.iut.model.Semester;
import edu.iut.model.Student;
import edu.iut.service.CourseManager;
import edu.iut.service.FacultyManager;
import edu.iut.service.GroupClassManager;
import edu.iut.service.GroupEducationManager;
import edu.iut.service.QuestionManager;
import edu.iut.service.QuestionTypeManager;
import edu.iut.service.StudentManager;

public class ApplicationConversionServiceFactoryBean extends FormattingConversionServiceFactoryBean {

	private ResourceBundleMessageSource messageSource;
	private GroupClassManager groupClassManager;
	private FacultyManager facultyManager;
	private StudentManager studentManager;
	private GroupEducationManager groupEducationManager;
	private CourseManager courseManager;
	private QuestionManager questionManager;
	private QuestionTypeManager questionTypeManager;

	@SuppressWarnings("deprecation")
	@Override
	protected void installFormatters(FormatterRegistry registry) {
		super.installFormatters(registry);
		// Register application converters and formatters
		registry.addConverter(getSemesterToStringConverter());
		registry.addConverter(getLevelToStringConverter());
		registry.addConverter(getGroupClassToStringConverter());
		registry.addConverter(getStringToGroupClassConverter());
		registry.addConverter(getStringToFacultyConverter());
		registry.addConverter(getFacultyToStringConverter());
		registry.addConverter(getStudentToStringConverter());
		registry.addConverter(getStringToStudentConverter());
		registry.addConverter(getStringToGroupEducationConverter());
		registry.addConverter(getGroupEducationToStringConverter());
		registry.addConverter(getStringToCourseConverter());
		registry.addConverter(getCourseToStringConverter());
		registry.addConverter(getStringToQuestionConverter());
		registry.addConverter(getQuestionToStringConverter());
		registry.addConverter(getStringToQuestionTypeConverter());
		registry.addConverter(getQuestionTypeToStringConverter());
	}

	private Converter<String, Question> getStringToQuestionConverter() {
		return new Converter<String, Question>() {
			@Override
			public Question convert(String source) {
				if (source.equalsIgnoreCase("null")) {
					return null;
				}
				return questionManager.get(new Long(source));
			}
		};
	}

	private Converter<Question, String> getQuestionToStringConverter() {
		return new Converter<Question, String>() {
			@Override
			public String convert(Question source) {
				return source.getId().toString();
			}
		};
	}

	private Converter<String, Student> getStringToStudentConverter() {
		return new Converter<String, Student>() {
			@Override
			public Student convert(String source) {
				return studentManager.get(new Long(source));
			}
		};
	}

	private Converter<Student, String> getStudentToStringConverter() {
		return new Converter<Student, String>() {
			@Override
			public String convert(Student source) {
				return source.getId().toString();
			}
		};
	}

	private Converter<String, Faculty> getStringToFacultyConverter() {
		return new Converter<String, Faculty>() {
			@Override
			public Faculty convert(String source) {
				if (source.equalsIgnoreCase("null")) {
					return null;
				}
				return facultyManager.get(new Long(source));
			}
		};
	}

	private Converter<Faculty, String> getFacultyToStringConverter() {
		return new Converter<Faculty, String>() {
			@Override
			public String convert(Faculty source) {
				return source.getId().toString();
			}
		};
	}

	private Converter<String, GroupClass> getStringToGroupClassConverter() {
		return new Converter<String, GroupClass>() {
			@Override
			public GroupClass convert(String source) {
				return groupClassManager.get(new Long(source));
			}
		};
	}

	private Converter<GroupClass, String> getGroupClassToStringConverter() {
		return new Converter<GroupClass, String>() {
			@Override
			public String convert(GroupClass source) {
				return source.getId().toString();
			}
		};
	}
	private Converter<String, GroupEducation> getStringToGroupEducationConverter() {
		return new Converter<String, GroupEducation>() {
			@Override
			public GroupEducation convert(String source) {
				return groupEducationManager.get(new Long(source));
			}
		};
	}
	
	private Converter<GroupEducation, String> getGroupEducationToStringConverter() {
		return new Converter<GroupEducation, String>() {
			@Override
			public String convert(GroupEducation source) {
				return source.getId().toString();
			}
		};
	}
	private Converter<String, Course> getStringToCourseConverter() {
		return new Converter<String, Course>() {
			@Override
			public Course convert(String source) {
				return courseManager.get(new Long(source));
			}
		};
	}
	
	private Converter<Course, String> getCourseToStringConverter() {
		return new Converter<Course, String>() {
			@Override
			public String convert(Course source) {
				return source.getId().toString();
			}
		};
	}
	
	private Converter<String, QuestionType> getStringToQuestionTypeConverter() {
		return new Converter<String, QuestionType>() {
			@Override
			public QuestionType convert(String source) {
				return questionTypeManager.get(new Long(source));
			}
		};
	}
	
	private Converter<QuestionType, String> getQuestionTypeToStringConverter() {
		return new Converter<QuestionType, String>() {
			@Override
			public String convert(QuestionType source) {
				return source.getId().toString();
			}
		};
	}
	
	
	private Converter<Semester, String> getSemesterToStringConverter() {
		return new Converter<Semester, String>() {
			@Override
			public String convert(Semester source) {
				return messageSource.getMessage(source.name(), new String[]{}, new Locale("fa", "IR"));
			}
		};
	}

	private Converter<Level, String> getLevelToStringConverter() {
		return new Converter<Level, String>() {
			@Override
			public String convert(Level source) {
				return messageSource.getMessage(source.name(), new String[]{}, new Locale("fa", "IR"));
			}
		};
	}

	public void setMessageSource(ResourceBundleMessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public void setGroupClassManager(GroupClassManager groupClassManager) {
		this.groupClassManager = groupClassManager;
	}

	public void setFacultyManager(FacultyManager facultyManager) {
		this.facultyManager = facultyManager;
	}

	public void setStudentManager(StudentManager studentManager) {
		this.studentManager = studentManager;
	}

	public void setGroupEducationManager(
			GroupEducationManager groupEducationManager) {
		this.groupEducationManager = groupEducationManager;
	}
	
	public void setCourseManager(CourseManager courseManager) {
		this.courseManager = courseManager;
	}

	public void setQuestionManager(QuestionManager questionManager) {
		this.questionManager = questionManager;
	}

	public void setQuestionTypeManager(QuestionTypeManager questionTypeManager) {
		this.questionTypeManager = questionTypeManager;
	}

}