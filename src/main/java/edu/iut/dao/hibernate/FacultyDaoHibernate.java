package edu.iut.dao.hibernate;

import org.springframework.stereotype.Repository;

import edu.iut.dao.FacultyDao;
import edu.iut.model.Faculty;

@Repository("facultyDao")
public class FacultyDaoHibernate extends GenericDaoHibernate<Faculty, Long> implements FacultyDao {

	public FacultyDaoHibernate() {
		super(Faculty.class);
	}

}