package edu.iut.dao.hibernate;

import org.springframework.stereotype.Repository;

import edu.iut.dao.QuestionTypeDao;
import edu.iut.model.QuestionType;

@Repository("questionTypeDao")
public class QuestionTypeDaoHibernate extends GenericDaoHibernate<QuestionType, Long> implements QuestionTypeDao {

	public QuestionTypeDaoHibernate() {
		super(QuestionType.class);
	}

}