package edu.iut.dao.hibernate;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import edu.iut.dao.StudentDao;
import edu.iut.model.Student;
import edu.iut.model.User;

@Repository("studentDao")
public class StudentDaoHibernate extends GenericDaoHibernate<Student, Long> implements StudentDao {

	public StudentDaoHibernate() {
		super(Student.class);
	}

	@Override
	public Student findByUser(User user) {
		List<Student> students = getSession().createCriteria(Student.class)
				.add(Restrictions.eq("user", user)).list();

		if (students == null || students.isEmpty()) {
			return null;
		}

		return students.get(0);
	}

}