package edu.iut.dao.hibernate;

import org.springframework.stereotype.Repository;

import edu.iut.dao.CourseDao;
import edu.iut.model.Course;

@Repository("courseDao")
public class CourseDaoHibernate extends GenericDaoHibernate<Course, Long> implements CourseDao {

	public CourseDaoHibernate() {
		super(Course.class);
	}

}