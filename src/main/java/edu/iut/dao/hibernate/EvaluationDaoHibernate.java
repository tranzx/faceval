package edu.iut.dao.hibernate;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import edu.iut.dao.EvaluationDao;
import edu.iut.model.Evaluation;
import edu.iut.model.Semester;

@Repository("evaluationDao")
public class EvaluationDaoHibernate extends GenericDaoHibernate<Evaluation, Long> implements EvaluationDao {

	/**
	 * @param persistentClass
	 */
	public EvaluationDaoHibernate() {
		super(Evaluation.class);
	}

	@Override
	public Evaluation saveEvaluation(Evaluation evaluation) {
		if (evaluation.getActive()) { // Evaluation is active - deactivate entire table before save
			getSession().createQuery("update Evaluation eval set eval.active=false").executeUpdate();
		}
		getSession().saveOrUpdate(evaluation);
		return evaluation;
	}

	@Override
	public Evaluation save(Evaluation evaluation) {
		return saveEvaluation(evaluation); // Override default save
	}

	@SuppressWarnings("unchecked")
	@Override
	public Evaluation findByYearAndSemester(String year, Semester semester) {
		List<Evaluation> evals = getSession().createCriteria(Evaluation.class)
				.add(Restrictions.eq("year", year))
				.add(Restrictions.eq("semester", semester)).list();

		if (evals == null || evals.isEmpty()) {
			return null;
		}

		return evals.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public Evaluation findCurrentActiveEvaluation() {
		List<Evaluation> evals = getSession().createQuery("from Evaluation eval where eval.active=true").list();

		if (evals == null || evals.size() != 1) {
			throw new RuntimeException("We doesn't have any active Evaluation! please contact Adminstrator of website to activate one.");
		}

		return evals.get(0);
	}

}