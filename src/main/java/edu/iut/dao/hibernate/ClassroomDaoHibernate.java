package edu.iut.dao.hibernate;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import edu.iut.dao.ClassroomDao;
import edu.iut.model.Classroom;
import edu.iut.model.GroupClass;

@Repository("classroomDao")
public class ClassroomDaoHibernate extends GenericDaoHibernate<Classroom, Long> implements ClassroomDao {

	public ClassroomDaoHibernate() {
		super(Classroom.class);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Classroom> findByGroupClass(GroupClass groupClass) {
		return getSession().createCriteria(Classroom.class)
				.add(Restrictions.eq("groupClass", groupClass)).list();
	}

}