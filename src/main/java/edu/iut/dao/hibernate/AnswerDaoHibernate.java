package edu.iut.dao.hibernate;

import java.util.List;

import org.springframework.stereotype.Repository;

import edu.iut.dao.AnswerDao;
import edu.iut.model.Answer;
import edu.iut.model.Evaluation;

@Repository("answerDao")
public class AnswerDaoHibernate extends GenericDaoHibernate<Answer, Long> implements AnswerDao {

	public AnswerDaoHibernate() {
		super(Answer.class);
	}

	@Override
	public List<Answer> findByEvaluation(Evaluation evaluation) {
		return getSession().createQuery("from Answer ans where ans.evaluation=:evaluation").setParameter("evaluation", evaluation).list();
	}

}