package edu.iut.dao.hibernate;

import org.springframework.stereotype.Repository;

import edu.iut.dao.GroupClassDao;
import edu.iut.model.GroupClass;

@Repository("groupClassDao")
public class GroupClassDaoHibernate extends GenericDaoHibernate<GroupClass, Long> implements GroupClassDao {

	public GroupClassDaoHibernate() {
		super(GroupClass.class);
	}

}