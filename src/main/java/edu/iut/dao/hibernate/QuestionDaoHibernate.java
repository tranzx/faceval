package edu.iut.dao.hibernate;

import org.springframework.stereotype.Repository;

import edu.iut.dao.QuestionDao;
import edu.iut.model.Question;

@Repository("questionDao")
public class QuestionDaoHibernate extends GenericDaoHibernate<Question, Long> implements QuestionDao {

	public QuestionDaoHibernate() {
		super(Question.class);
	}

}