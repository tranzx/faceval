package edu.iut.dao.hibernate;

import org.springframework.stereotype.Repository;

import edu.iut.dao.GroupEducationDao;
import edu.iut.model.GroupEducation;

@Repository("groupEducationDao")
public class GroupEducationDaoHibernate extends GenericDaoHibernate<GroupEducation, Long> implements GroupEducationDao {

	public GroupEducationDaoHibernate() {
		super(GroupEducation.class);
	}

}