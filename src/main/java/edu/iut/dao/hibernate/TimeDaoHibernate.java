package edu.iut.dao.hibernate;

import org.springframework.stereotype.Repository;

import edu.iut.dao.TimeDao;
import edu.iut.model.Time;

@Repository("timeDao")
public class TimeDaoHibernate extends GenericDaoHibernate<Time, Long> implements TimeDao {

	public TimeDaoHibernate() {
		super(Time.class);
	}

}