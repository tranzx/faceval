package edu.iut.dao;

import edu.iut.model.Time;

/**
 * Student Data Access Object interface.
 */
public interface TimeDao extends GenericDao<Time, Long> {

}