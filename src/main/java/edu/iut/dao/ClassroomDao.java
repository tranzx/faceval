package edu.iut.dao;

import java.util.List;

import edu.iut.model.Classroom;
import edu.iut.model.GroupClass;

/**
 * ClassRoom Data Access Object interface.
 */
public interface ClassroomDao extends GenericDao<Classroom, Long> {

	List<Classroom> findByGroupClass(GroupClass groupClass);

}