package edu.iut.dao;

import java.util.List;

import edu.iut.model.Answer;
import edu.iut.model.Evaluation;

public interface AnswerDao extends GenericDao<Answer, Long> {

	List<Answer> findByEvaluation(Evaluation evaluation);

}