package edu.iut.dao;

import edu.iut.model.Evaluation;
import edu.iut.model.Semester;

/**
 * Evaluation Data Access Object (GenericDao) interface.
 *
 */
public interface EvaluationDao extends GenericDao<Evaluation, Long> {

	Evaluation saveEvaluation(Evaluation evaluation);

	Evaluation findByYearAndSemester(String year, Semester semester);

	Evaluation findCurrentActiveEvaluation();

}