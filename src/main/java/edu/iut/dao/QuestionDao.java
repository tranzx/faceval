package edu.iut.dao;

import edu.iut.model.Question;

/**
 * Question Data Access Object interface.
 */
public interface QuestionDao extends GenericDao<Question, Long> {

}