package edu.iut.dao;

import edu.iut.model.GroupEducation;

/**
 * GroupEducation Data Access Object interface.
 */
public interface GroupEducationDao extends GenericDao<GroupEducation, Long> {

}