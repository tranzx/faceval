package edu.iut.dao;

import edu.iut.model.Faculty;

/**
 * Faculty Data Access Object interface.
 */
public interface FacultyDao extends GenericDao<Faculty, Long> {

}