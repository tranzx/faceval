package edu.iut.dao;

import edu.iut.model.GroupClass;

/**
 * GroupClass Data Access Object interface.
 */
public interface GroupClassDao extends GenericDao<GroupClass, Long> {

}