package edu.iut.dao;

import edu.iut.model.QuestionType;

/**
 * QuestionType Data Access Object interface.
 */
public interface QuestionTypeDao extends GenericDao<QuestionType, Long> {

}