package edu.iut.dao;

import edu.iut.model.Course;

/**
 * Course Data Access Object interface.
 */
public interface CourseDao extends GenericDao<Course, Long> {

}