package edu.iut.dao;

import edu.iut.model.Student;
import edu.iut.model.User;

/**
 * Student Data Access Object interface.
 */
public interface StudentDao extends GenericDao<Student, Long> {

	Student findByUser(User user);

}