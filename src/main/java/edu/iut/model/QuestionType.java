package edu.iut.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "questiontype")
public class QuestionType extends BaseObject {

	private static final long serialVersionUID = -8989971107496767021L;
	private Long id;
	private String name;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	@Column(nullable = false)
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name  ;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof QuestionType)) {
			return false;
		}
		final QuestionType qtype = (QuestionType) o;

		return !(name != null ? !toString().equals(qtype.toString()) : qtype.getName() != null);
	}

	@Override
	public int hashCode() {
		return (name != null ? toString().hashCode() : 0);
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

}
