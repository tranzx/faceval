package edu.iut.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "groupeducation")
public class GroupEducation extends BaseObject {

	private static final long serialVersionUID = -2516153426075248256L;
	private Long id;
	private String name;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	@Column(nullable = false)
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name  ;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof GroupEducation)) {
			return false;
		}
		final GroupEducation groupEdu = (GroupEducation) o;

		return !(name != null ? !toString().equals(groupEdu.toString()) : groupEdu.getName() != null);
	}

	@Override
	public int hashCode() {
		return (name != null ? toString().hashCode() : 0);
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

}
