package edu.iut.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "question")
public class Question extends BaseObject implements Serializable {
	private static final long serialVersionUID = -8028431185077693608L;

	private Long id;
	private String text;
	private QuestionType questionType;

	public Question() {}

	public Question(final String text) {
		this.text = text;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	@Column(nullable = false)
	public String getText() {
		return text;
	}

	@JoinColumn(name = "questiontype_id", nullable = false)
	@ManyToOne(targetEntity = QuestionType.class, optional = false)	
	public QuestionType getQuestionType() {
		return questionType;
	}
	
	@Override
	public String toString() {
		return text;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if(!(o instanceof Question)) {
			return false;
		}
		final Question question = (Question) o;
		if (this.id != null) {
			return this.id.equals(question.id);
		}
		return !(text != null ? !text.equals(question.getText()) : question.getText() != null);
	}

	@Override
	public int hashCode() {
		if (id != null) {
			return id.hashCode();
		}
		return (text != null ? text.hashCode() : 0);
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setText(String text) {
		this.text = text;
	}

	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}
}