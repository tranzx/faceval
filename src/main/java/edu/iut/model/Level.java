package edu.iut.model;

public enum Level {
	EMPTY,WEAK,MIDDLE,GOOD,VERYGOOD,GREAT;

	public int num() {
		switch(this) {
		case WEAK:
			return 1;
		case MIDDLE:
			return 2;
		case GOOD:
			return 3;
		case VERYGOOD:
			return 4;
		case GREAT:
			return 5;
		case EMPTY:
			return 0;
		default:
			return 0;
		}
	}

}