package edu.iut.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "time")
public class Time extends BaseObject {

	private static final long serialVersionUID = -1973722335895639182L;
	private Long id;
	private String term;
	private String year;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	@Column(nullable = false)
	public String getTerm() {
		return term;
	}
	
	@Column(nullable = false)
	public String getYear() {
		return year;
	}



	@Override
	public String toString() {
		return term + ", " + year ;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Time)) {
			return false;
		}
		final Time time = (Time) o;

		return !(term != null ? !toString().equals(time.toString()) : time.getTerm() != null);
	}

	@Override
	public int hashCode() {
		return (term != null ? toString().hashCode() : 0);
	}

	public void setId(Long id) {
		this.id = id;
	}


	public void setTerm(String term) {
		this.term = term;
	}


	public void setYear(String year) {
		this.year = year;
	}




}
