package edu.iut.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "classroom")
//, uniqueConstraints = 
//{@UniqueConstraint(columnNames = {"groupclass_id", "faculty_id", "course_id"}, name = "classroomUniqueTableConstraint")})
public class Classroom extends BaseObject {
	private static final long serialVersionUID = 247469695530123578L;

	private Long id;
	private Course course;
	private GroupClass groupClass;
	private Faculty faculty;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	@JoinColumn(name = "groupclass_id", nullable = false )
	@ManyToOne(targetEntity = GroupClass.class, optional = false)
	public GroupClass getGroupClass() {
		return groupClass;
	}

	@JoinColumn(name = "faculty_id", nullable = false)
	@ManyToOne(targetEntity = Faculty.class)
	public Faculty getFaculty() {
		return faculty;
	}
	
	@JoinColumn(name = "course_id", nullable = false)
	@ManyToOne(targetEntity = Course.class)
	public Course getCourse() {
		return course;
	}

	@Override
	public String toString() {
		return groupClass + ", " + course + ", " + faculty;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof Classroom)) {
			return false;
		}
		final Classroom classRoom = (Classroom) o;

		return !(course != null ? !toString().equals(classRoom.toString()) : classRoom.getCourse() != null);
	}

	@Override
	public int hashCode() {
		return (course != null ? toString().hashCode() : 0);
	}
	public void setId(Long id) {
		this.id = id;
	}

	public void setGroupClass(GroupClass groupClass) {
		this.groupClass = groupClass;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}
	
	public void setCourse(Course course) {
		this.course = course;
	}

}