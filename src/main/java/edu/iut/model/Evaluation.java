package edu.iut.model;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "evaluation")
public class Evaluation extends BaseObject implements Serializable {
	private static final long serialVersionUID = 8048880901799826601L;

	private Long id;
	private String name;
	private Boolean active;
	private Date createdAt;
	private Integer version;
	private String year;
	private Semester semester;
	private Set<Question> questions = new HashSet<Question>();
	private Set<Faculty> faculties = new HashSet<Faculty>();

	public Evaluation() {}

	public Evaluation(final String name) {
		this.name = name;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	
	@Column(nullable = false)
	public String getYear() {
		return year;
	}

	@Enumerated(EnumType.STRING)
	public Semester getSemester() {
		return semester;
	}

	@Column(nullable = false)
	public String getName() {
		return name;
	}

	@Column(nullable = true)
	public Boolean getActive() {
		return active;
	}

	@Column(nullable = false)
	public Date getCreatedAt() {
		return createdAt;
	}

	@Version
	public Integer getVersion() {
		return version;
	}

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "evaluation_question",
			joinColumns = { @JoinColumn(name = "evaluation_id") },
			inverseJoinColumns = @JoinColumn(name = "question_id")
	)
	public Set<Question> getQuestions() {
		return questions;
	}
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(
			name = "evaluation_faculty",
			joinColumns = { @JoinColumn(name = "evaluation_id") },
			inverseJoinColumns = @JoinColumn(name = "faculty_id")
			)	
	public Set<Faculty> getFaculties() {
		return faculties;
	}

	@Override
	public String toString() {
		return name + ", " + year;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if(!(o instanceof Evaluation)) {
			return false;
		}
		final Evaluation eval = (Evaluation) o;

		if (this.id != null) {
			return this.id.equals(eval.id);
		}
		return !(name != null ? !name.equals(eval.getName()) : eval.getName() != null);
	}

	@Override
	public int hashCode() {
		if (id != null) {
			return id.hashCode();
		}
		return (name != null ? name.hashCode() : 0);
	}

	public void addQuestion(Question question) {
		getQuestions().add(question);
	}
	
	public void addFaculty(Faculty faculty) {
		getFaculties().add(faculty);
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void setVersion(Integer version) {
		this.version = version;
	}

	public void setQuestions(Set<Question> questions) {
		this.questions = questions;
	}

	public void setFaculties(Set<Faculty> faculties) {
		this.faculties = faculties;
	}

	public void setSemester(Semester semester) {
		this.semester = semester;
	}
	
	public void setYear(String year) {
		this.year = year;
	}
}