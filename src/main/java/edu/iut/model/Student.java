package edu.iut.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "student")
public class Student extends BaseObject {

	private static final long serialVersionUID = 8842038971922579187L;
	
	private Long id;
	private String name;
	private String family;

	private User user;
	private GroupClass groupClass;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	@JoinColumn(name = "user_id", nullable = false)
	@ManyToOne(targetEntity = User.class, optional = false)
	public User getUser() {
		return user;
	}

	@JoinColumn(name = "groupclass_id", nullable = false)
	@ManyToOne(targetEntity = GroupClass.class, optional = false)
	public GroupClass getGroupClass() {
		return groupClass;
	}

	@Transient
	public String getName() {
		return user.getFirstName();
	}

	@Transient
	public String getFamily() {
		return user.getLastName();
	}

	@Transient
	public String getFullname() {
		return user.getFullName();
	}

	@Override
	public String toString() {
		return getFullname();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Student) {
			final Student that = (Student) obj;
			if (id != null && that.getId() != null) {
				return id.equals(that.getId());
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return (user != null ? user.hashCode() : 0);
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setGroupClass(GroupClass groupClass) {
		this.groupClass = groupClass;
	}

}