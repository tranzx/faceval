package edu.iut.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "faculty")
public class Faculty extends BaseObject {

	private static final long serialVersionUID = -5262017111523217323L;
	private Long id;
	private String name;
	private String family;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	@Column(nullable = false)
	public String getName() {
		return name;
	}

	@Column(nullable = false)
	public String getFamily() {
		return family;
	}

	@Transient
	public String getFullname() {
		return name + " " + family;
	}

	@Override
	public String toString() {
		return name + " " + family ;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Faculty) {
			final Faculty that = (Faculty) obj;
			if (id != null && that.getId() != null) {
				return id.equals(that.getId());
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return (name != null ? toString().hashCode() : 0);
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void setFamily(String family) {
		this.family = family;
	}


}
