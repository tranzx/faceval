package edu.iut.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "groupclass")
public class GroupClass extends BaseObject {

	private static final long serialVersionUID = 837360983387043255L;
	private Long id;
	private String name;
	private GroupEducation groupEducation;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	@Column(nullable = false)
	public String getName() {
		return name;
	}

	@JoinColumn(name = "groupeducation_id", nullable = false)
	@ManyToOne(targetEntity = GroupEducation.class, optional = false)	
	public GroupEducation getGroupEducation() {
		return groupEducation;
	}

	@Override
	public String toString() {
		return name  ;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof GroupClass)) {
			return false;
		}
		final GroupClass groupClasses = (GroupClass) o;

		return !(name != null ? !toString().equals(groupClasses.toString()) : groupClasses.getName() != null);
	}

	@Override
	public int hashCode() {
		return (name != null ? toString().hashCode() : 0);
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setGroupEducation(GroupEducation groupEducation) {
		this.groupEducation = groupEducation;
	}
	
}
