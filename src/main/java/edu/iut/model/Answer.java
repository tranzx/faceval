package edu.iut.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "answer", uniqueConstraints = {@UniqueConstraint(columnNames = {"question_id", "faculty_id", "student_id"}, name = "answerUniqueTableConstraint")})
public class Answer {

	private Long id;
	private Level level;

	private Question question;
	private Evaluation evaluation;
//	private Classroom classroom; TODO ?
	private Faculty faculty;
	private Student student;


	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	@Enumerated(value = EnumType.STRING)
	@Column(name = "level", nullable = false)
	public Level getLevel() {
		return level;
	}

	@JoinColumn(name = "question_id", nullable = false)
	@ManyToOne(targetEntity = Question.class, optional = false)
	public Question getQuestion() {
		return question;
	}

	@JoinColumn(name = "faculty_id", nullable = false)
	@ManyToOne(targetEntity = Faculty.class, optional = false)
	public Faculty getFaculty() {
		return faculty;
	}
	@JoinColumn(name = "evaluation_id", nullable = false)
	@ManyToOne(targetEntity = Evaluation.class, optional = false)
	public Evaluation getEvaluation() {
		return evaluation;
	}

	@JoinColumn(name = "student_id", nullable = false)
	@ManyToOne(targetEntity = Student.class, optional = false)
	public Student getStudent() {
		return student;
	}

	@Override
	public String toString() {
		return level.name().toLowerCase();
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Answer) {
			final Answer that = (Answer) obj;
			if (id != null && that.getId() != null) {
				return id.equals(that.getId());
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return (id != null ? id.hashCode() : 0);
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setLevel(Level level) {
		this.level = level;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public void setFaculty(Faculty faculty) {
		this.faculty = faculty;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public void setEvaluation(Evaluation evaluation) {
		this.evaluation = evaluation;
	}
}